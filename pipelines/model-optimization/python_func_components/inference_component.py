import kfp
from kfp.components import InputTextFile
from kfp.v2 import dsl
from kfp.v2.dsl import component
from kfp.v2.dsl import (
    Input,
    Output,
    Artifact,
    Dataset,
    OutputPath,
    InputPath
)

def inference(
    macro_file: Input[Dataset],
    output_root_file="/tmp/outputs/data",#: OutputPath(str),
):
    return dsl.ContainerOp(
        name="Inference",
        image="gitlab-registry.cern.ch/prmehta/geant4_par04/full-ort-par04:latest",
        command=[
            "/opt/par04/examplePar04",
            "-m", f"{macro_file.path}",
            "-o", f"{output_root_file}",
        ],
        file_outputs={
            "output_root_file": output_root_file,            
        }
    )