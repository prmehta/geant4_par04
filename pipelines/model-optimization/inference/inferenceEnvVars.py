from kubernetes import client as k8s_client

INF_PATH = k8s_client.V1EnvVar(
    name='PATH',
    value='/opt/root/src/root/bin:/opt/geant4/install/bin:/opt/tensorrt/bin:/usr/local/mpi/bin:/usr/local/nvidia/bin:/usr/local/cuda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/ucx/bin:/opt/onednn/install/bin')

INF_LD_LIBRARY_PATH = k8s_client.V1EnvVar(
    name='LD_LIBRARY_PATH',
    value='/opt/root/src/root/lib:/opt/geant4/install/lib:/usr/local/cuda/compat/lib:/usr/local/nvidia/lib:/usr/local/nvidia/lib64:/opt/intel/openvino/extras/opencv/lib:/opt/intel/openvino/runtime/lib/intel64:/opt/intel/openvino/tools/compile_tool:/opt/intel/openvino/runtime/3rdparty/tbb/lib:/opt/intel/openvino/runtime/3rdparty/hddl/lib:/opt/onnxruntime/install/lib:/opt/onednn/install/include:/opt/onednn/install/lib')

INF_G4ENSDFSTATEDATA = k8s_client.V1EnvVar(
    name="G4ENSDFSTATEDATA",
    value="/opt/geant4/data/G4ENSDFSTATE2.3")

INF_G4PIIDATA = k8s_client.V1EnvVar(
    name="G4PIIDATA",
    value="/opt/geant4/data/G4PII1.3")

INF_TBB_DIR = k8s_client.V1EnvVar(
    name="TBB_DIR",
    value="/opt/intel/openvino/runtime/3rdparty/tbb/cmake")

INF_G4INCLDATA = k8s_client.V1EnvVar(
    name="G4INCLDATA",
    value="/opt/geant4/data/G4INCL1.0")

INF_InferenceEngine_DIR = k8s_client.V1EnvVar(
    name="InferenceEngine_DIR",
    value="/opt/intel/openvino/runtime/cmake")

INF_G4LEDATA=k8s_client.V1EnvVar(
    name="G4LEDATA",
    value="/opt/geant4/data/G4EMLOW8.0")

INF_OpenVINO_DIR = k8s_client.V1EnvVar(
    name="OpenVINO_DIR",
    value="/opt/intel/openvino/runtime/cmake")

INF_G4PARTICLEXSDATA = k8s_client.V1EnvVar(
    name="G4PARTICLEXSDATA",
    value="/opt/geant4/data/G4PARTICLEXS4.0")

INF_INTEL_OPENVINO_DIR = k8s_client.V1EnvVar(
    name="INTEL_OPENVINO_DIR",
    value="/opt/intel/openvino")

INF_OpenCV_DIR = k8s_client.V1EnvVar(
    name="OpenCV_DIR",
    value="/opt/intel/openvino/extras/opencv/cmake")

INF_G4NEUTRONHPDATA = k8s_client.V1EnvVar(
    name="G4NEUTRONHPDATA",
    value="/opt/geant4/data/G4NDL4.6")

INF_G4SAIDXSDATA = k8s_client.V1EnvVar(
    name="G4SAIDXSDATA",
    value="/opt/geant4/data/G4SAIDDATA2.0")

INF_ngraph_DIR = k8s_client.V1EnvVar(
    name="ngraph_DIR",
    value="/opt/intel/openvino/runtime/cmake")

INF_G4REALSURFACEDATA = k8s_client.V1EnvVar(
    name="G4REALSURFACEDATA",
    value="/opt/geant4/data/RealSurface2.2")

INF_G4ABLADATA = k8s_client.V1EnvVar(
    name="G4ABLADATA",
    value="/opt/geant4/data/G4ABLA3.1")

INF_G4LEVELGAMMADATA = k8s_client.V1EnvVar(
    name="G4LEVELGAMMADATA",
    value="/opt/geant4/data/PhotonEvaporation5.7")

INF_G4RADIOACTIVEDATA = k8s_client.V1EnvVar(
    name="G4RADIOACTIVEDATA",
    value="/opt/geant4/data/RadioactiveDecay5.6")

INF_HDDL_INSTALL_DIR = k8s_client.V1EnvVar(
    name="HDDL_INSTALL_DIR",
    value="/opt/intel/openvino/runtime/3rdparty/hddl")