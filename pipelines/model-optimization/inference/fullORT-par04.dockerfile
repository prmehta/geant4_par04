ARG TRT_CONTAINER_VERSION=21.12
FROM nvcr.io/nvidia/tensorrt:${TRT_CONTAINER_VERSION}-py3 as base

# Copyright (C) 2019-2022 Intel Corporation
# SPDX-License-Identifier: Apache-2.0
# FROM ubuntu:20.04 AS base

# hadolint ignore=DL3002
USER root
WORKDIR /

SHELL ["/bin/bash", "-xo", "pipefail", "-c"]

ENV DEBIAN_FRONTEND=noninteractive

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install -y --no-install-recommends curl tzdata ca-certificates wget && \
    rm -rf /var/lib/apt/lists/*


# get product from URL
ARG package_url=https://storage.openvinotoolkit.org/repositories/openvino/packages/2022.1/l_openvino_toolkit_dev_ubuntu20_p_2022.1.0.643.tgz
ARG TEMP_DIR=/tmp/openvino_installer

WORKDIR ${TEMP_DIR}
# hadolint ignore=DL3020
RUN wget ${package_url}

# install product by copying archive content
ARG TEMP_DIR=/tmp/openvino_installer
ENV INTEL_OPENVINO_DIR /opt/intel/openvino

RUN tar -xzf "${TEMP_DIR}"/*.tgz && \
    OV_BUILD="$(find . -maxdepth 1 -type d -name "*openvino*" | grep -oP '(?<=_)\d+.\d+.\d.\d+')" && \
    OV_YEAR="$(find . -maxdepth 1 -type d -name "*openvino*" | grep -oP '(?<=_)\d+')" && \
    OV_FOLDER="$(find . -maxdepth 1 -type d -name "*openvino*")" && \
    mkdir -p /opt/intel/openvino_"$OV_BUILD"/ && \
    cp -rf "$OV_FOLDER"/*  /opt/intel/openvino_"$OV_BUILD"/ && \
    rm -rf "${TEMP_DIR:?}"/"$OV_FOLDER" && \
    ln --symbolic /opt/intel/openvino_"$OV_BUILD"/ /opt/intel/openvino && \
    ln --symbolic /opt/intel/openvino_"$OV_BUILD"/ /opt/intel/openvino_"$OV_YEAR" && \
    rm -rf ${INTEL_OPENVINO_DIR}/tools/workbench && rm -rf ${TEMP_DIR}


ENV HDDL_INSTALL_DIR=/opt/intel/openvino/runtime/3rdparty/hddl
ENV InferenceEngine_DIR=/opt/intel/openvino/runtime/cmake
ENV LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/opt/intel/openvino/extras/opencv/lib:/opt/intel/openvino/runtime/lib/intel64:/opt/intel/openvino/tools/compile_tool:/opt/intel/openvino/runtime/3rdparty/tbb/lib:/opt/intel/openvino/runtime/3rdparty/hddl/lib
ENV OpenCV_DIR=/opt/intel/openvino/extras/opencv/cmake

ENV PYTHONPATH=/opt/intel/openvino/python/python3.8:/opt/intel/openvino/python/python3:/opt/intel/openvino/extras/opencv/python
ENV TBB_DIR=/opt/intel/openvino/runtime/3rdparty/tbb/cmake
ENV ngraph_DIR=/opt/intel/openvino/runtime/cmake
ENV OpenVINO_DIR=/opt/intel/openvino/runtime/cmake

# for VPU
ARG BUILD_DEPENDENCIES="autoconf \
                        automake \
                        build-essential \
                        libtool \
                        unzip"

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install -y --no-install-recommends ${BUILD_DEPENDENCIES} && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /opt
RUN curl -L https://github.com/libusb/libusb/archive/v1.0.22.zip --output v1.0.22.zip && \
    unzip v1.0.22.zip && rm -rf v1.0.22.zip

WORKDIR /opt/libusb-1.0.22
RUN ./bootstrap.sh && \
    ./configure --disable-udev --enable-shared && \
    make -j4

RUN rm -rf ${INTEL_OPENVINO_DIR}/.distribution && mkdir ${INTEL_OPENVINO_DIR}/.distribution && \
    touch ${INTEL_OPENVINO_DIR}/.distribution/docker

################################################################################################################################################################   
### ------------------------------------------------------------INSTALLING OPENVINO ALONGSIDE CUDA AND TENSORRT----------------------------------------------###
################################################################################################################################################################

ARG TRT_CONTAINER_VERSION=21.12
FROM nvcr.io/nvidia/tensorrt:${TRT_CONTAINER_VERSION}-py3 as ep_base

LABEL description="This is the dev image for Intel(R) Distribution of OpenVINO(TM) toolkit on Ubuntu 20.04 LTS"
LABEL vendor="Intel Corporation"

USER root
WORKDIR /

SHELL ["/bin/bash", "-xo", "pipefail", "-c"]

ENV DEBIAN_FRONTEND=noninteractive

# Creating user openvino and adding it to groups "video" and "users" to use GPU and VPU
RUN sed -ri -e 's@^UMASK[[:space:]]+[[:digit:]]+@UMASK 000@g' /etc/login.defs && \
	grep -E "^UMASK" /etc/login.defs && \
	useradd -ms /bin/bash -G video,users -d /workspace/openvino openvino && \
	chown openvino -R /workspace/openvino

RUN mkdir /opt/intel

ENV INTEL_OPENVINO_DIR /opt/intel/openvino

COPY --from=base /opt/intel /opt/intel

WORKDIR /thirdparty

ARG INSTALL_SOURCES="no"

ARG DEPS="tzdata \
          curl"

ARG LGPL_DEPS="g++ \
               gcc \
               libc6-dev"
ARG INSTALL_PACKAGES="-c=opencv_req -c=python -c=cl_compiler"


# hadolint ignore=DL3008
RUN apt-get update && \
    dpkg --get-selections | grep -v deinstall | awk '{print $1}' > base_packages.txt  && \
    apt-get install -y --no-install-recommends ${DEPS} && \
    rm -rf /var/lib/apt/lists/*

# hadolint ignore=DL3008, SC2012
RUN apt-get update && \
    apt-get install -y --no-install-recommends ${LGPL_DEPS} && \
    ${INTEL_OPENVINO_DIR}/install_dependencies/install_openvino_dependencies.sh -y ${INSTALL_PACKAGES} && \
    if [ "$INSTALL_SOURCES" = "yes" ]; then \
      sed -Ei 's/# deb-src /deb-src /' /etc/apt/sources.list && \
      apt-get update && \
	  dpkg --get-selections | grep -v deinstall | awk '{print $1}' > all_packages.txt && \
	  grep -v -f base_packages.txt all_packages.txt | while read line; do \
	  package=$(echo $line); \
	  name=(${package//:/ }); \
      grep -l GPL /usr/share/doc/${name[0]}/copyright; \
      exit_status=$?; \
	  if [ $exit_status -eq 0 ]; then \
	    apt-get source -q --download-only $package;  \
	  fi \
      done && \
      echo "Download source for $(ls | wc -l) third-party packages: $(du -sh)"; fi && \
    rm /usr/lib/python3.*/lib-dynload/readline.cpython-3*-gnu.so && rm -rf /var/lib/apt/lists/*

WORKDIR ${INTEL_OPENVINO_DIR}/licensing
RUN if [ "$INSTALL_SOURCES" = "no" ]; then \
        echo "This image doesn't contain source for 3d party components under LGPL/GPL licenses. Please use tag <YYYY.U_src> to pull the image with downloaded sources." > DockerImage_readme.txt ; \
    fi


ENV HDDL_INSTALL_DIR=/opt/intel/openvino/runtime/3rdparty/hddl
ENV InferenceEngine_DIR=/opt/intel/openvino/runtime/cmake
ENV LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/opt/intel/openvino/extras/opencv/lib:/opt/intel/openvino/runtime/lib/intel64:/opt/intel/openvino/tools/compile_tool:/opt/intel/openvino/runtime/3rdparty/tbb/lib:/opt/intel/openvino/runtime/3rdparty/hddl/lib
ENV OpenCV_DIR=/opt/intel/openvino/extras/opencv/cmake

ENV PYTHONPATH=/opt/intel/openvino/python/python3.8:/opt/intel/openvino/python/python3:/opt/intel/openvino/extras/opencv/python
ENV TBB_DIR=/opt/intel/openvino/runtime/3rdparty/tbb/cmake
ENV ngraph_DIR=/opt/intel/openvino/runtime/cmake
ENV OpenVINO_DIR=/opt/intel/openvino/runtime/cmake

# setup Python
ENV PYTHON_VER python3.8

RUN ${PYTHON_VER} -m pip install --upgrade pip

# dev package
WORKDIR ${INTEL_OPENVINO_DIR}
ARG OPENVINO_WHEELS_VERSION=2022.1.0
ARG OPENVINO_WHEELS_URL
# hadolint ignore=SC2102
RUN ${PYTHON_VER} -m pip install --no-cache-dir cmake && \
    if [ -z "$OPENVINO_WHEELS_URL" ]; then \
        ${PYTHON_VER} -m pip install --no-cache-dir openvino=="$OPENVINO_WHEELS_VERSION" && \
        ${PYTHON_VER} -m pip install --no-cache-dir openvino_dev[caffe,kaldi,mxnet,onnx,pytorch,tensorflow2]=="$OPENVINO_WHEELS_VERSION" ; \
    else \
        ${PYTHON_VER} -m pip install --no-cache-dir --pre openvino=="$OPENVINO_WHEELS_VERSION" --trusted-host=* --find-links "$OPENVINO_WHEELS_URL" && \
        ${PYTHON_VER} -m pip install --no-cache-dir --pre openvino_dev[caffe,kaldi,mxnet,onnx,pytorch,tensorflow2]=="$OPENVINO_WHEELS_VERSION" --trusted-host=* --find-links "$OPENVINO_WHEELS_URL" ; \
    fi

WORKDIR ${INTEL_OPENVINO_DIR}/licensing
# Please use `third-party-programs-docker-dev.txt` short path to 3d party file if you use the Dockerfile directly from docker_ci/dockerfiles repo folder
COPY ./third-party-programs-docker-dev.txt ${INTEL_OPENVINO_DIR}/licensing
COPY ./third-party-programs-docker-runtime.txt ${INTEL_OPENVINO_DIR}/licensing

# for CPU

# for GPU
ARG TEMP_DIR=/tmp/opencl

RUN apt-get update && \ 
	mkdir -p ${TEMP_DIR} && \
	wget https://github.com/intel/compute-runtime/releases/download/22.16.22992/libigdgmm12_22.1.2_amd64.deb && \
	wget https://github.com/intel/intel-graphics-compiler/releases/download/igc-1.0.10988/intel-igc-core_1.0.10988_amd64.deb && \
	wget https://github.com/intel/intel-graphics-compiler/releases/download/igc-1.0.10988/intel-igc-opencl_1.0.10988_amd64.deb && \
	wget https://github.com/intel/compute-runtime/releases/download/22.16.22992/intel-opencl-icd-dbgsym_22.16.22992_amd64.ddeb && \
	wget https://github.com/intel/compute-runtime/releases/download/22.16.22992/intel-opencl-icd_22.16.22992_amd64.deb && \
	wget https://github.com/intel/compute-runtime/releases/download/22.16.22992/intel-level-zero-gpu-dbgsym_1.3.22992_amd64.ddeb && \
	wget https://github.com/intel/compute-runtime/releases/download/22.16.22992/intel-level-zero-gpu_1.3.22992_amd64.deb && \
	wget https://github.com/intel/compute-runtime/releases/download/22.16.22992/ww16.sum && \
	sha256sum -c ww16.sum && \
	apt-get install -y ./*.deb && \
	rm -rf /var/lib/apt/lists/*

# for VPU
ARG LGPL_DEPS=udev

WORKDIR /thirdparty

# hadolint ignore=DL3008, SC2012
RUN apt-get update && \
    dpkg --get-selections | grep -v deinstall | awk '{print $1}' > no_vpu_packages.txt && \
    apt-get install -y --no-install-recommends ${LGPL_DEPS} && \
    if [ "$INSTALL_SOURCES" = "yes" ]; then \
      sed -Ei 's/# deb-src /deb-src /' /etc/apt/sources.list && \
      apt-get update && \
	  dpkg --get-selections | grep -v deinstall | awk '{print $1}' > vpu_packages.txt && \
	  grep -v -f no_vpu_packages.txt vpu_packages.txt | while read line; do \
	  package=$(echo $line); \
	  name=(${package//:/ }); \
      grep -l GPL /usr/share/doc/${name[0]}/copyright; \
      exit_status=$?; \
	  if [ $exit_status -eq 0 ]; then \
	    apt-get source -q --download-only $package;  \
	  fi \
      done && \
      echo "Download source for $(ls | wc -l) third-party packages: $(du -sh)"; fi && \
    rm -rf /var/lib/apt/lists/*

COPY --from=base /opt/libusb-1.0.22 /opt/libusb-1.0.22

WORKDIR /opt/libusb-1.0.22/libusb
RUN /bin/mkdir -p '/usr/local/lib' && \
    /bin/bash ../libtool   --mode=install /usr/bin/install -c   libusb-1.0.la '/usr/local/lib' && \
    /bin/mkdir -p '/usr/local/include/libusb-1.0' && \
    /usr/bin/install -c -m 644 libusb.h '/usr/local/include/libusb-1.0' && \
    /bin/mkdir -p '/usr/local/lib/pkgconfig'

WORKDIR /opt/libusb-1.0.22/
RUN /usr/bin/install -c -m 644 libusb-1.0.pc '/usr/local/lib/pkgconfig' && \
    cp ${INTEL_OPENVINO_DIR}/runtime/3rdparty/97-myriad-usbboot.rules /etc/udev/rules.d/ && \
    ldconfig

# for HDDL
WORKDIR /tmp
# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        libboost-filesystem-dev \
        libboost-program-options-dev \
        libboost-thread-dev \
        libjson-c4 \
        libxxf86vm-dev && \
    rm -rf /var/lib/apt/lists/* && rm -rf /tmp/*


# Post-installation cleanup and setting up OpenVINO environment variables
ENV LIBVA_DRIVER_NAME=iHD
ENV GST_VAAPI_ALL_DRIVERS=1
ENV LIBVA_DRIVERS_PATH=/usr/lib/x86_64-linux-gnu/dri

RUN apt-get update && \
    apt-get autoremove -y gfortran && \
    rm -rf /var/lib/apt/lists/*

#############################################################################################################################################################
###--------------------------------------------------INSTALLING ONEDNN ALONGSIDE OPENVINO, CUDA and TensorRT----------------------------------------------###
#############################################################################################################################################################

RUN mkdir -p /opt/onednn/src && \
    mkdir -p /opt/onednn/build && \
    mkdir -p /opt/onednn/install && \
    git clone -b rls-v2.6 https://github.com/oneapi-src/oneDNN.git /opt/onednn/src
    
RUN cd /opt/onednn/build && \
	cmake -DCMAKE_INSTALL_PREFIX=/opt/onednn/install \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_GPU_RUNTIME=OCL \
		-DONEDNN_ENABLE_WORKLOAD=INFERENCE \
		-DONEDNN_BUILD_EXAMPLES=OFF \
		-DONEDNN_BUILD_TESTS=OFF\
		../src
RUN cd /opt/onednn/build && \
    make -j`nproc` && \
    make install

###########################################################################################################################################################
### ------------------------------------------------------------------INSTALLING ONNXRUNTIME------------------------------------------------------------###
###########################################################################################################################################################

USER root

ENV DEBIAN_FRONTEND=noninteractive

ARG ORT_VERSION=1.11.0
ARG ORT_REPO=https://github.com/microsoft/onnxruntime.git
ARG OPENVINO_DEVICE=CPU_FP32 
ARG CMAKE_CUDA_ARCHITECTURES=37;50;52;60;61;70;75;80;86
ARG CMAKE_VERSION=3.21.0

RUN apt-get update -y && \
    TZ=Etc/UTC apt-get -y install tzdata && \
    apt-get -y install build-essential libtool autoconf unzip wget curl gcc g++ libexpat1-dev software-properties-common git python3-dev python3-numpy python3-setuptools python3-wheel python3-pip aria2

RUN aria2c -q -d /tmp -o cmake-${CMAKE_VERSION}-linux-x86_64.tar.gz https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}-linux-x86_64.tar.gz && \
        tar -zxf /tmp/cmake-${CMAKE_VERSION}-linux-x86_64.tar.gz --strip=1 -C /usr/local

RUN mkdir -p /opt/onnxruntime/src && \
    mkdir -p /opt/onnxruntime/build && \
    mkdir -p /opt/onnxruntime/install

RUN cd /opt/onnxruntime/src && \
	git clone --recursive -b rel-${ORT_VERSION} ${ORT_REPO} /opt/onnxruntime/src/ && \
	/bin/sh dockerfiles/scripts/install_common_deps.sh

RUN cd /opt/onnxruntime/src && \
    ./build.sh --config Release --update --build --parallel --build_wheel --build_shared_lib --build_dir /opt/onnxruntime/build \
    	--cmake_extra_defines CMAKE_INSTALL_PREFIX=/opt/onnxruntime/install ONNXRUNTIME_VERSION=$(cat ./VERSION_NUMBER) '"CMAKE_CUDA_ARCHITECTURES='${CMAKE_CUDA_ARCHITECTURES}'"' \
    	--use_dnnl \
    	--use_openvino ${OPENVINO_DEVICE} \
    	--use_cuda --cuda_home /usr/local/cuda --cudnn_home /usr/lib/x86_64-linux-gnu/ \
    	--use_tensorrt --tensorrt_home /usr/lib/x86_64-linux-gnu/ \
    	--skip_tests --skip_submodule_sync
    	
RUN cd /opt/onnxruntime/build/Release && \
    make -j`nproc` && \
    make install

RUN python3 -m pip install /opt/onnxruntime/build/Release/dist/*.whl

#RUN mkdir -p /models && \
#    cd /models && \
#    wget -O resnet.tar.gz https://github.com/onnx/models/raw/main/vision/classification/resnet/model/resnet18-v1-7.tar.gz && \
#    tar -xvf resnet.tar.gz
    
##############################################################################################################################################################
###----------------------------------------------------------------------BUILDING GEANT-4------------------------------------------------------------------###
##############################################################################################################################################################

FROM ep_base as g4_builder

ENV G4_VERSION 11.0

ARG G4_URL=https://gitlab.cern.ch/geant4/geant4.git

RUN apt-get update -y && \
    apt-get install -y libexpat1 libexpat1-dev
    
RUN mkdir -p /opt/geant4/src && \
    mkdir -p /opt/geant4/build && \
    mkdir -p /opt/geant4/install && \
    mkdir -p /opt/geant4/data && \
    git clone --recursive -b geant4-${G4_VERSION}-release ${G4_URL} /opt/geant4/src
    
RUN cd /opt/geant4/build && \
    cmake -DCMAKE_INSTALL_PREFIX=/opt/geant4/install \
          -DGEANT4_INSTALL_DATA=ON \
          -DGEANT4_INSTALL_DATADIR=/opt/geant4/data \
          -DGEANT4_BUILD_MULTITHREADED=ON \
          -DGEANT4_INSTALL_EXAMPLES=OFF \
          -DGEANT4_USE_SYSTEM_EXPAT=ON \
          ../src && \
    make -j`nproc` && \
    make install

#############################################################################################################################################################
###---------------------------------------------------------------INSTALLING ROOT-CERN--------------------------------------------------------------------###
#############################################################################################################################################################

FROM g4_builder as root_builder

ARG root_v=6.26.00
ARG os_v=Linux-ubuntu20
ARG architecure_v=x86_64
ARG gcc_v=gcc9.3

RUN mkdir -p /opt/root/src && \
	cd /opt/root/src && \
	wget https://root.cern/download/root_v${root_v}.${os_v}-${architecure_v}-${gcc_v}.tar.gz && \
	tar -xzvf root_v${root_v}.${os_v}-${architecure_v}-${gcc_v}.tar.gz && \
	source root/bin/thisroot.sh && \
	rm -rf root_v${root_v}.${os_v}-${architecure_v}-${gcc_v}.tar.gz

##############################################################################################################################################################
###----------------------------------------------------------------BUILDING PAR04 EXAMPLE------------------------------------------------------------------###
##############################################################################################################################################################

FROM root_builder as par04_builder

RUN source /opt/geant4/install/bin/geant4.sh

ARG CACHEBUST=1

RUN mkdir -p /opt/par04/src

ARG PAR04_URL=https://gitlab.cern.ch/prmehta/geant4_par04.git

RUN git clone --recursive -b par04-gsoc-priyam ${PAR04_URL} /opt/par04/src

RUN cd /opt/par04/src && \
    rm -rf CMakeCache.txt CMakeFiles examplePar04 Makefile && \
    cmake -DCMAKE_BUILD_TYPE="Debug" \
          -DCMAKE_PREFIX_PATH="/opt/root/src/root;/opt/geant4/install;/opt/onnxruntime/install;/opt/onnxruntime/data;/usr/local/cuda;" \
          -DCMAKE_CXX_FLAGS_DEBUG="-ggdb3" \
    	  .

RUN cd /opt/par04/src && \
    make -j`nproc` && \
    make install

##############################################################################################################################################################
###-------------------------------------------------------INSTALLING GEANT-4 and PAR-04 ALONGSIDE ORT------------------------------------------------------###
##############################################################################################################################################################

FROM ep_base as final_base

USER root

COPY --from=g4_builder /opt/geant4/data /opt/geant4/data
COPY --from=g4_builder /opt/geant4/install /opt/geant4/install
COPY --from=root_builder /opt/root/src/root /opt/root/src/root
COPY --from=par04_builder /opt/par04/src /opt/par04

ENV LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/opt/onnxruntime/install/lib:/opt/onednn/install/include:/opt/onednn/install/lib
ENV PATH=${PATH}:/opt/onednn/install/bin

RUN echo ". /opt/geant4/install/bin/geant4.sh" >> ~/.bashrc
RUN echo ". /opt/root/src/root/bin/thisroot.sh" >> ~/.bashrc

RUN mkdir -p /opt/trt/geant4/cache

RUN rm -rf /opt/onnxruntime/src && \
	rm -rf /opt/onnxruntime/build && \
	rm -rf /opt/onednn/src && \
	rm -rf /opt/onednn/build && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /opt/par04

CMD [ "/bin/bash" ]
