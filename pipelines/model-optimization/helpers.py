import kfp
from inference.inferenceEnvVars import *

def get_macro_handler(
    mh_url="https://gitlab.cern.ch/prmehta/geant4_par04/-/raw/par04-gsoc-priyam/pipelines/model-optimization/macrohandler/macro-handler-component.yaml",
    mh_file="macrohandler/macro-handler-component.yaml",
    url=True,
):
    """
    Loads the macro handler component from URL or file.
    Macro Handler component was built to give flexibility to
    the user for updating macro file templates. It converts jsonlines
    to .mac files and passes it to the connect inference component.
    """
    if url:
        return kfp.components.load_component_from_url(mh_url)
    else:
        return kfp.components.load_component_from_file(mh_file)

def get_benchmark(
    b_url="https://gitlab.cern.ch/prmehta/geant4_par04/-/raw/par04-gsoc-priyam/pipelines/model-optimization/benchmark/benchmark-component.yaml",
    b_file="benchmark/benchmark-component.yaml",
    url=True,
):
    """
    Loads the benchmark component from URL or file.
    Benchmark component process the output .root files and sends
    compares different .root files to get similarity measures like KL Divergence etc.
    """
    if url:
        return kfp.components.load_component_from_url(b_url)
    else:
        return kfp.components.load_component_from_file(b_file)

def get_inference(
    i_url="https://gitlab.cern.ch/prmehta/geant4_par04/-/raw/par04-gsoc-priyam/pipelines/model-optimization/inference/inference-component.yaml",
    i_file="inference/inference-component.yaml",
    url=True,
):
    """
    Loads the inference component from URL or file.
    Inference component contains the Par04-FastSim C++ module,
    which takes in the input macrofile from macro-handler component.
    """
    if url:
        return kfp.components.load_component_from_url(i_url)
    else:
        return kfp.components.load_component_from_file(i_file)
    

def configure_inference_task(macro_file, output_root_filename,
                             component_display_name, use_url=False,
                             gpu_limit=None):
    
    inference = get_inference(url=use_url)
    
    if gpu_limit:
        inference_task = inference(macro_file=macro_file,
                                   output_root_filename=output_root_filename) \
                                        .add_env_variable(INF_PATH) \
                                        .add_env_variable(INF_LD_LIBRARY_PATH) \
                                        .add_env_variable(INF_G4ENSDFSTATEDATA) \
                                        .add_env_variable(INF_G4PIIDATA) \
                                        .add_env_variable(INF_TBB_DIR) \
                                        .add_env_variable(INF_G4INCLDATA) \
                                        .add_env_variable(INF_InferenceEngine_DIR) \
                                        .add_env_variable(INF_G4LEDATA) \
                                        .add_env_variable(INF_OpenVINO_DIR) \
                                        .add_env_variable(INF_G4PARTICLEXSDATA) \
                                        .add_env_variable(INF_INTEL_OPENVINO_DIR) \
                                        .add_env_variable(INF_OpenCV_DIR) \
                                        .add_env_variable(INF_G4NEUTRONHPDATA) \
                                        .add_env_variable(INF_G4SAIDXSDATA) \
                                        .add_env_variable(INF_ngraph_DIR) \
                                        .add_env_variable(INF_G4REALSURFACEDATA) \
                                        .add_env_variable(INF_G4ABLADATA) \
                                        .add_env_variable(INF_G4LEVELGAMMADATA) \
                                        .add_env_variable(INF_G4RADIOACTIVEDATA) \
                                        .add_env_variable(INF_HDDL_INSTALL_DIR) \
                                        .set_gpu_limit(gpu_limit) \
                                        .set_display_name(component_display_name)
    
    else:
        inference_task = inference(macro_file=macro_file,
                                   output_root_filename=output_root_filename) \
                                    .add_env_variable(INF_PATH) \
                                    .add_env_variable(INF_LD_LIBRARY_PATH) \
                                    .add_env_variable(INF_G4ENSDFSTATEDATA) \
                                    .add_env_variable(INF_G4PIIDATA) \
                                    .add_env_variable(INF_TBB_DIR) \
                                    .add_env_variable(INF_G4INCLDATA) \
                                    .add_env_variable(INF_InferenceEngine_DIR) \
                                    .add_env_variable(INF_G4LEDATA) \
                                    .add_env_variable(INF_OpenVINO_DIR) \
                                    .add_env_variable(INF_G4PARTICLEXSDATA) \
                                    .add_env_variable(INF_INTEL_OPENVINO_DIR) \
                                    .add_env_variable(INF_OpenCV_DIR) \
                                    .add_env_variable(INF_G4NEUTRONHPDATA) \
                                    .add_env_variable(INF_G4SAIDXSDATA) \
                                    .add_env_variable(INF_ngraph_DIR) \
                                    .add_env_variable(INF_G4REALSURFACEDATA) \
                                    .add_env_variable(INF_G4ABLADATA) \
                                    .add_env_variable(INF_G4LEVELGAMMADATA) \
                                    .add_env_variable(INF_G4RADIOACTIVEDATA) \
                                    .add_env_variable(INF_HDDL_INSTALL_DIR) \
                                    .set_display_name(component_display_name)
           
    return inference_task