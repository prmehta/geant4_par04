ARG TRT_CONTAINER_VERSION=21.12
FROM nvcr.io/nvidia/tensorrt:${TRT_CONTAINER_VERSION}-py3 as ort_builder

ARG ONNXRUNTIME_REPO=https://github.com/Microsoft/onnxruntime
ARG ONNXRUNTIME_BRANCH=master
ARG CMAKE_CUDA_ARCHITECTURES=37;50;52;60;61;70;75;80;86

##############################################################################################################################
###--------------------------------------------------BUILDING ONNXRUNTIME--------------------------------------------------###
##############################################################################################################################

USER root

ENV DEBIAN_FRONTEND=noninteractive

ARG ORT_VERSION=1.11.0
ARG ORT_REPO=https://github.com/microsoft/onnxruntime.git
ARG OPENVINO_DEVICE=CPU_FP32 

RUN apt-get update -y && \
    TZ=Etc/UTC apt-get -y install tzdata && \
    apt-get -y install build-essential libtool autoconf unzip wget curl gcc g++ libexpat1-dev software-properties-common git python3-dev python3-numpy python3-setuptools python3-wheel python3-pip aria2

RUN aria2c -q -d /tmp -o cmake-3.21.0-linux-x86_64.tar.gz https://github.com/Kitware/CMake/releases/download/v3.21.0/cmake-3.21.0-linux-x86_64.tar.gz && \
        tar -zxf /tmp/cmake-3.21.0-linux-x86_64.tar.gz --strip=1 -C /usr/local

RUN mkdir -p /opt/onnxruntime/src && \
    mkdir -p /opt/onnxruntime/build && \
    mkdir -p /opt/onnxruntime/install

RUN cd /opt/onnxruntime/src && \
	git clone --recursive -b rel-${ORT_VERSION} ${ORT_REPO} /opt/onnxruntime/src/ && \
	/bin/sh dockerfiles/scripts/install_common_deps.sh

RUN cd /opt/onnxruntime/src && \
    ./build.sh --config Release --update --build --parallel --build_wheel --build_shared_lib --build_dir /opt/onnxruntime/build \
    	--cmake_extra_defines CMAKE_INSTALL_PREFIX=/opt/onnxruntime/install ONNXRUNTIME_VERSION=$(cat ./VERSION_NUMBER) '"CMAKE_CUDA_ARCHITECTURES='${CMAKE_CUDA_ARCHITECTURES}'"' \
    	--use_cuda --cuda_home /usr/local/cuda --cudnn_home /usr/lib/x86_64-linux-gnu/ \
    	--use_tensorrt --tensorrt_home /usr/lib/x86_64-linux-gnu/ \
    	--skip_tests --skip_submodule_sync
    	
RUN cd /opt/onnxruntime/build/Release && \
    make -j`nproc` && \
    make install
    
RUN mkdir -p /opt/onnxruntime/install/pywheels && \
    cp -r /opt/onnxruntime/build/Release/dist/*.whl /opt/onnxruntime/install/pywheels

##############################################################################################################################
###-----------------------------------------------------BUILDING GEANT4----------------------------------------------------###
##############################################################################################################################

FROM ort_builder as g4_builder

ENV G4_VERSION 11.0

#ARG G4_URL=https://gitlab.cern.ch/geant4/geant4/-/archive/v${G4_VERSION}/geant4-v${G4_VERSION}.tar.gz

ARG G4_URL=https://gitlab.cern.ch/geant4/geant4.git

RUN apt-get update -y && \
    apt-get install -y libexpat1 libexpat1-dev
    
RUN mkdir -p /opt/geant4/src && \
    mkdir -p /opt/geant4/build && \
    mkdir -p /opt/geant4/install && \
    mkdir -p /opt/geant4/data && \
    git clone --recursive -b geant4-${G4_VERSION}-release ${G4_URL} /opt/geant4/src
    #curl -o /geant4.tar.gz ${G4_URL} && \
    #tar xf /geant4.tar.gz -C /opt/geant4/src
    
RUN ls /opt/geant4/src
    
RUN cd /opt/geant4/build && \
    cmake -DCMAKE_INSTALL_PREFIX=/opt/geant4/install \
          -DGEANT4_INSTALL_DATA=ON \
          -DGEANT4_INSTALL_DATADIR=/opt/geant4/data \
          -DGEANT4_BUILD_MULTITHREADED=ON \
          -DGEANT4_INSTALL_EXAMPLES=OFF \
          -DGEANT4_USE_SYSTEM_EXPAT=ON \
          #../src/geant4-v${G4_VERSION} && \
          ../src && \
    make -j`nproc` && \
    make install
    
RUN source /opt/geant4/install/bin/geant4.sh

##############################################################################################################################
###-------------------------------------------------INSTALLING ROOT-CERN---------------------------------------------------###
##############################################################################################################################

FROM g4_builder as root_builder

ARG root_v=6.26.00
ARG os_v=Linux-ubuntu20
ARG architecure_v=x86_64
ARG gcc_v=gcc9.3

RUN mkdir -p /opt/root/src && \
	cd /opt/root/src && \
	wget https://root.cern/download/root_v${root_v}.${os_v}-${architecure_v}-${gcc_v}.tar.gz && \
	tar -xzvf root_v${root_v}.${os_v}-${architecure_v}-${gcc_v}.tar.gz && \
	source root/bin/thisroot.sh && \
	rm -rf root_v${root_v}.${os_v}-${architecure_v}-${gcc_v}.tar.gz

RUN cd /opt/root/src && \
	ls .

##############################################################################################################################
###-----------------------------------------------------BUILDING PAR04-----------------------------------------------------###
##############################################################################################################################

FROM root_builder as par04_builder

RUN mkdir -p /opt/par04/src

ARG PAR04_URL=https://gitlab.cern.ch/prmehta/geant4_par04.git

RUN git clone --recursive ${PAR04_URL} /opt/par04/src

RUN cd /opt/par04/src && \
    rm -rf CMakeCache.txt CMakeFiles examplePar04 Makefile && \
    cmake -DCMAKE_BUILD_TYPE="Debug" \
          -DCMAKE_PREFIX_PATH="/opt/root/src/root;/opt/geant4/install;/opt/onnxruntime/install;/opt/onnxruntime/data;/usr/local/cuda;" \
          -DCMAKE_CXX_FLAGS_DEBUG="-ggdb3" \
    	  .

RUN cd /opt/par04/src && \
    make -j`nproc` && \
    make install

#############################################################################################################################
###-------------------------------------------------------CLEANING UP-----------------------------------------------------###
#############################################################################################################################

FROM nvcr.io/nvidia/tensorrt:${TRT_CONTAINER_VERSION}-py3 as finalBuild

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && \
    TZ=Etc/UTC apt-get -y install tzdata && \
    apt-get -y install build-essential gcc g++ wget curl git python3-dev python3-numpy python3-setuptools python3-wheel python3-pip
    #apt-get -y install build-essential libtool autoconf unzip wget curl gcc g++ libexpat1-dev software-properties-common git python3-dev python3-numpy python3-setuptools python3-wheel python3-pip aria2
        
RUN mkdir -p /opt/onnxruntime/install && \
    mkdir -p /opt/root/src/root && \
	mkdir -p /opt/geant4/install && \
	mkdir -p /opt/geant4/data && \
	mkdir -p /opt/Par04 && \
	mkdir -p /opt/trt/geant4/cache

COPY --from=ort_builder /opt/onnxruntime/install /opt/onnxruntime/install
COPY --from=root_builder /opt/root/src/root /opt/root/src/root
COPY --from=g4_builder /opt/geant4/install /opt/geant4/install
COPY --from=g4_builder /opt/geant4/data /opt/geant4/data
COPY --from=par04_builder /opt/par04/src /opt/par04

RUN echo ". /opt/geant4/install/bin/geant4.sh" >> ~/.bashrc
RUN echo ". /opt/root/src/root/bin/thisroot.sh" >> ~/.bashrc

ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/onnxruntime/install/lib;

RUN rm -rf /var/lib/apt/lists/*

RUN cd /opt/onnxruntime/install/pywheels && \
	python3 -m pip install ./*.whl 

WORKDIR /opt/par04
							   #####
###-------------------------------------------------------#END#-----------------------------------------------------------###
							   #####	
							    
							
