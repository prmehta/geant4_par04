import jsonlines
from collections import OrderedDict
from functools import partial
import requests
import numpy as np
import os
import kfp
from kfp.v2 import dsl
from kfp.v2.dsl import component
from kfp.v2.dsl import (
    Input,
    Output,
    Artifact,
    Dataset,
    OutputPath,
    InputPath
)

@component(
    packages_to_install=["jsonlines", "requests", "numpy"],
    output_component_file="macro-handler-component.yaml",
)
def macroHandler(
    jsonlines_url:str,
    jsonlines_path: InputPath(str),
    macro_file_path: OutputPath(str),
    particle_energy:str ="10 GeV",
    particle_angle:int =90,
    set_size_latent_vector:int =10,
    set_size_condition_vector:int =4,
    set_model_path_name:str ="./MLModels/Generator.onnx",
    set_profile_flag:int =1,
    set_optimization_flag:int =0,
    set_dnnl_flag:int =0,
    set_openvino_flag:int =0,
    set_cuda_flag:int =0,
    set_tensorrt_flag:int =0,
    set_dnnl_enable_cpu_mem_arena:int =1,
    set_cuda_device_id:int =0,
    set_cuda_gpu_mem_limit:str ="2147483648",
    set_cuda_arena_extended_strategy:str ="kSameAsRequested",
    set_cuda_cudnn_conv_algo_search:str ="DEFAULT",
    set_cuda_do_copy_in_default_stream:int =1,
    set_cuda_cudnn_conv_use_max_workspace:int =1,
    set_trt_device_id:int =0,
    set_trt_max_workspace_size:str ="2147483648",
    set_trt_max_partition_iterations:int =10,
    set_trt_min_subgraph_size:int =5,
    set_trt_fp16_enable:int =0,
    set_trt_int8_enable:int =0,
    set_trt_int8_use_native_calibration_table:int =1,
    set_trt_engine_cache_enable:int =1,
    set_trt_engine_cache_path:str ="/opt/trt/geant4/cache",
    set_trt_dump_subgraphs:int =1,
    set_ov_device_type:str ="CPU_FP32",
    set_ov_enable_vpu_fast_compile:int =0,
    set_ov_num_of_threads:int =1,
    set_ov_use_compiled_network:int =0,
    set_ov_enable_opencl_throttling:int =0,
    set_inference_library:str ="ONNX",
#                     setFileName="10GeV_1000events_fastsim_onnx.root",
    beam_on:int =1000,
):
    def commandInterface(command, value):
        
        command_dict = OrderedDict([
            # Inference Setup
            ## dimension of the latent vector (encoded vector in a Variational Autoencoder model)
            ("/gun/energy", particle_energy),
            ("/gun/direction", f"0 {np.sin(np.deg2rad(particle_angle))} {np.cos(np.deg2rad(particle_angle))}"),
            ("/Par04/inference/setSizeLatentVector", set_size_latent_vector),
            ## size of the condition vector (energy, angle and geometry)  
            ("/Par04/inference/setSizeConditionVector", set_size_condition_vector),
            ## path to the model which is set to download by cmake
            ("/Par04/inference/setModelPathName", set_model_path_name),
            ("/Par04/inference/setProfileFlag", set_profile_flag),
            ("/Par04/inference/setOptimizationFlag", set_optimization_flag),
            ("/Par04/inference/setDnnlFlag", set_dnnl_flag),
            ("/Par04/inference/setOpenVinoFlag", set_openvino_flag),
            ("/Par04/inference/setCudaFlag", set_cuda_flag),
            ("/Par04/inference/setTensorrtFlag", set_tensorrt_flag),

            # onednn options
            ("/Par04/inference/onednn/setEnableCpuMemArena", set_dnnl_enable_cpu_mem_arena),

            # cuda options
            ("/Par04/inference/cuda/setDeviceId", set_cuda_device_id),
            ("/Par04/inference/cuda/setGpuMemLimit", set_cuda_gpu_mem_limit),
            ("/Par04/inference/cuda/setArenaExtendedStrategy", set_cuda_arena_extended_strategy),
            ("/Par04/inference/cuda/setCudnnConvAlgoSearch", set_cuda_cudnn_conv_algo_search),
            ("/Par04/inference/cuda/setDoCopyInDefaultStream", set_cuda_do_copy_in_default_stream),
            ("/Par04/inference/cuda/setCudnnConvUseMaxWorkspace", set_cuda_cudnn_conv_use_max_workspace),

            # tensorrt options
            ("/Par04/inference/trt/setDeviceId", set_trt_device_id),
            ("/Par04/inference/trt/setMaxWorkspaceSize", set_trt_max_workspace_size),
            ("/Par04/inference/trt/setMaxPartitionIterations", set_trt_max_partition_iterations),
            ("/Par04/inference/trt/setMinSubgraphSize", set_trt_min_subgraph_size),
            ("/Par04/inference/trt/setFp16Enable", set_trt_fp16_enable),
            ("/Par04/inference/trt/setInt8Enable", set_trt_int8_enable),
            ("/Par04/inference/trt/setInt8UseNativeCalibrationTable", set_trt_int8_use_native_calibration_table),
            ("/Par04/inference/trt/setEngineCacheEnable", set_trt_engine_cache_enable),
            ("/Par04/inference/trt/setEngineCachePath", set_trt_engine_cache_path),
            ("/Par04/inference/trt/setDumpSubgraphs", set_trt_dump_subgraphs),

            # openvino options
            ("/Par04/inference/openvino/setDeviceType", set_ov_device_type),
            ("/Par04/inference/openvino/setEnableVpuFastCompile", set_ov_enable_vpu_fast_compile),
            #/Par04/inference/openvino/setDeviceId 
            ("/Par04/inference/openvino/setNumOfThreads", set_ov_num_of_threads),
            ("/Par04/inference/openvino/setUseCompiledNetwork", set_ov_use_compiled_network),
            #/Par04/inference/openvino/setBlobDumpPath 
            ("/Par04/inference/openvino/setEnableOpenCLThrottling", set_ov_enable_opencl_throttling),
            
            ("/Par04/inference/setInferenceLibrary", set_inference_library),

            # Fast Simulation
    #        ("/analysis/setFileName", setFileName),
            ("/run/beamOn", beam_on),
        ])
        return command_dict.get(command, value) 

    def jsonlinesToMacro(jsonlines_url, input_json_lines, output_path, macroCommandInterface):
        response = requests.get(jsonlines_url)
        with open(input_json_lines, "wb") as jsonl_file:
            jsonl_file.write(response.content)
            
        os.makedirs(os.path.dirname(output_path), exist_ok=True)
            
        with jsonlines.open(input_json_lines) as jsonlinesFile, open(output_path, "w") as macroFile:
            for macro in jsonlinesFile.iter(type=dict, skip_invalid=True):
                macro['value'] = macroCommandInterface(macro['command'], macro['value'])
                macroFile.write(f"{macro['command']} {macro['value'] if macro['value'] else ''}\n")
                
    
    jsonlinesToMacro(jsonlines_url, jsonlines_path, macro_file_path, commandInterface)
