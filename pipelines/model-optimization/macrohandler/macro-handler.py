import parser_handler
import jsonlines
from collections import OrderedDict
from functools import partial
import requests
import numpy as np
import os

def commandInterface(command, value,
                    particleEnergy:str ="10 GeV",
                    particleAngle:int =90,
                    setSizeLatentVector:int =10,
                    setSizeConditionVector:int =4,
                    setModelPathName:str ="./MLModels/Generator.onnx",
                    setProfileFlag:bool =True,
                    setOptimizationFlag:bool =False,
                    setDnnlFlag:bool =False,
                    setOpenVinoFlag:bool =False,
                    setCudaFlag:bool =False,
                    setTensorrtFlag:bool =False,
                    setDnnlEnableCpuMemArena:bool =True,
                    setCudaDeviceId:int =0,
                    setCudaGpuMemLimit:str ="2147483648",
                    setCudaArenaExtendedStrategy:str ="kSameAsRequested",
                    setCudaCudnnConvAlgoSearch:str ="DEFAULT",
                    setCudaDoCopyInDefaultStream:bool =True,
                    setCudaCudnnConvUseMaxWorkspace:int =1,
                    setTrtDeviceId:int =0,
                    setTrtMaxWorkspaceSize:str ="2147483648",
                    setTrtMaxPartitionIterations:int =10,
                    setTrtMinSubgraphSize:int =5,
                    setTrtFp16Enable:bool =False,
                    setTrtInt8Enable:bool =False,
                    setTrtInt8UseNativeCalibrationTable:bool =True,
                    setTrtEngineCacheEnable:bool =True,
                    setTrtEngineCachePath:str ="/opt/trt/geant4/cache",
                    setTrtDumpSubgraphs:int =1,
                    setOVDeviceType:str ="CPU_FP32",
                    setOVEnableVpuFastCompile:bool =False,
                    setOVNumOfThreads:int =1,
                    setOVUseCompiledNetwork:bool =False,
                    setOVEnableOpenCLThrottling:bool =False,
                    setInferenceLibrary:str ="ONNX",
                #    setFileName:str ="/opt/10GeV_1000events_fastsim_onnx.root",
                    beamOn:int =1000,
                     ):
    
    command_dict = OrderedDict([
        # Inference Setup
        ## dimension of the latent vector (encoded vector in a Variational Autoencoder model)
        ("/gun/energy", particleEnergy),
        ("/gun/direction", f"0 {np.sin(np.deg2rad(particleAngle))} {np.cos(np.deg2rad(particleAngle))}"),
        ("/Par04/inference/setSizeLatentVector", setSizeLatentVector),
        ## size of the condition vector (energy, angle and geometry)  
        ("/Par04/inference/setSizeConditionVector", setSizeConditionVector),
        ## path to the model which is set to download by cmake
        ("/Par04/inference/setModelPathName", setModelPathName),
        ("/Par04/inference/setProfileFlag", int(setProfileFlag)),
        ("/Par04/inference/setOptimizationFlag", int(setOptimizationFlag)),
        ("/Par04/inference/setDnnlFlag", int(setDnnlFlag)),
        ("/Par04/inference/setOpenVinoFlag", int(setOpenVinoFlag)),
        ("/Par04/inference/setCudaFlag", int(setCudaFlag)),
        ("/Par04/inference/setTensorrtFlag", int(setTensorrtFlag)),

        # onednn options
        ("/Par04/inference/onednn/setEnableCpuMemArena", int(setDnnlEnableCpuMemArena)),

        # cuda options
        ("/Par04/inference/cuda/setDeviceId", setCudaDeviceId),
        ("/Par04/inference/cuda/setGpuMemLimit", setCudaGpuMemLimit),
        ("/Par04/inference/cuda/setArenaExtendedStrategy", setCudaArenaExtendedStrategy),
        ("/Par04/inference/cuda/setCudnnConvAlgoSearch", setCudaCudnnConvAlgoSearch),
        ("/Par04/inference/cuda/setDoCopyInDefaultStream", int(setCudaDoCopyInDefaultStream)),
        ("/Par04/inference/cuda/setCudnnConvUseMaxWorkspace", setCudaCudnnConvUseMaxWorkspace),

        # tensorrt options
        ("/Par04/inference/trt/setDeviceId", setTrtDeviceId),
        ("/Par04/inference/trt/setMaxWorkspaceSize", setTrtMaxWorkspaceSize),
        ("/Par04/inference/trt/setMaxPartitionIterations", setTrtMaxPartitionIterations),
        ("/Par04/inference/trt/setMinSubgraphSize", setTrtMinSubgraphSize),
        ("/Par04/inference/trt/setFp16Enable", int(setTrtFp16Enable)),
        ("/Par04/inference/trt/setInt8Enable", int(setTrtInt8Enable)),
        ("/Par04/inference/trt/setInt8UseNativeCalibrationTable", int(setTrtInt8UseNativeCalibrationTable)),
        ("/Par04/inference/trt/setEngineCacheEnable", int(setTrtEngineCacheEnable)),
        ("/Par04/inference/trt/setEngineCachePath", setTrtEngineCachePath),
        ("/Par04/inference/trt/setDumpSubgraphs", setTrtDumpSubgraphs),

        # openvino options
        ("/Par04/inference/openvino/setDeviceType", setOVDeviceType),
        ("/Par04/inference/openvino/setEnableVpuFastCompile", int(setOVEnableVpuFastCompile)),
        #/Par04/inference/openvino/setDeviceId 
        ("/Par04/inference/openvino/setNumOfThreads", setOVNumOfThreads),
        ("/Par04/inference/openvino/setUseCompiledNetwork", int(setOVUseCompiledNetwork)),
        #/Par04/inference/openvino/setBlobDumpPath 
        ("/Par04/inference/openvino/setEnableOpenCLThrottling", int(setOVEnableOpenCLThrottling)),
        
        ("/Par04/inference/setInferenceLibrary", setInferenceLibrary),

        # Fast Simulation
#        ("/analysis/setFileName", setFileName),
        ("/run/beamOn", beamOn),
    ])
    value = command_dict.get(command, value)
    print("Output Value: ", value)
    return value

def jsonlinesToMacro(jsonlines_url, input_path, output_path, macroCommandInterface):
    
    response = requests.get(jsonlines_url)
    with open(input_path, "wb") as jsonl_file:
        jsonl_file.write(response.content)
        
    os.makedirs(os.path.dirname(output_path), exist_ok=True)
        
    with jsonlines.open(input_path) as jsonlinesFile, open(output_path, "w") as macroFile:
        for macro in jsonlinesFile.iter(type=dict, skip_invalid=True):
            print("Current Json: ", macro)
            print("Value Type:", type(macro["value"]))
            macro['value'] = macroCommandInterface(macro['command'], macro['value'])
            line_to_be_written = f"{macro['command']} {macro['value'] if macro['value'] != None else ''}\n"
            macroFile.write(line_to_be_written)
            print(line_to_be_written)
        
            

if __name__ == '__main__':
    args = vars(parser_handler.parse_args())
    print(args)
    
    macroCommandInterface = partial(commandInterface,
                                    particleEnergy=args["particleEnergy"],
                                    particleAngle=args["particleAngle"],
                                    setSizeLatentVector=args["setSizeLatentVector"],
                                    setSizeConditionVector=args["setSizeConditionVector"],
                                    setModelPathName=args["setModelPathName"],
                                    setProfileFlag=args["setProfileFlag"],
                                    setOptimizationFlag=args["setOptimizationFlag"],
                                    setDnnlFlag=args["setDnnlFlag"],
                                    setOpenVinoFlag=args["setOpenVinoFlag"],
                                    setCudaFlag=args["setCudaFlag"],
                                    setTensorrtFlag=args["setTensorrtFlag"],
                                    setDnnlEnableCpuMemArena=args["setDnnlEnableCpuMemArena"],
                                    setCudaDeviceId=args["setCudaDeviceId"],
                                    setCudaGpuMemLimit=args["setCudaGpuMemLimit"],
                                    setCudaArenaExtendedStrategy=args["setCudaArenaExtendedStrategy"],
                                    setCudaCudnnConvAlgoSearch=args["setCudaCudnnConvAlgoSearch"],
                                    setCudaDoCopyInDefaultStream=args["setCudaDoCopyInDefaultStream"],
                                    setCudaCudnnConvUseMaxWorkspace=args["setCudaCudnnConvUseMaxWorkspace"],
                                    setTrtDeviceId=args["setTrtDeviceId"],
                                    setTrtMaxWorkspaceSize=args["setTrtMaxWorkspaceSize"],
                                    setTrtMaxPartitionIterations=args["setTrtMaxPartitionIterations"],
                                    setTrtMinSubgraphSize=args["setTrtMinSubgraphSize"],
                                    setTrtFp16Enable=args["setTrtFp16Enable"],
                                    setTrtInt8Enable=args["setTrtInt8Enable"],
                                    setTrtInt8UseNativeCalibrationTable=args["setTrtInt8UseNativeCalibrationTable"],
                                    setTrtEngineCacheEnable=args["setTrtEngineCacheEnable"],
                                    setTrtEngineCachePath=args["setTrtEngineCachePath"],
                                    setTrtDumpSubgraphs=args["setTrtDumpSubgraphs"],
                                    setOVDeviceType=args["setOVDeviceType"],
                                    setOVEnableVpuFastCompile=args["setOVEnableVpuFastCompile"],
                                    setOVNumOfThreads=args["setOVNumOfThreads"],
                                    setOVUseCompiledNetwork=args["setOVUseCompiledNetwork"],
                                    setOVEnableOpenCLThrottling=args["setOVEnableOpenCLThrottling"],
                                    setInferenceLibrary=args["setInferenceLibrary"],
#                                    setFileName=args["setFileName"],
                                    beamOn=args["beamOn"])
    
    jsonlinesToMacro(args["jsonl_url"], args["input_path"], args["output_path"], macroCommandInterface)
    