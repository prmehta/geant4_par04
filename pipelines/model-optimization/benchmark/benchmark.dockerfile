FROM python:slim-buster as baseImage

USER root

ARG WORKDIR_PATH=/opt/benchmark

WORKDIR ${WORKDIR_PATH}

RUN pip install numpy matplotlib uproot awkward seaborn scipy

COPY . ${WORKDIR_PATH}