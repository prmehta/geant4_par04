ARG OPENVINO_VERSION=2022.1.0


# Build stage
FROM openvino/ubuntu18_runtime:${OPENVINO_VERSION} AS ort_builder

ENV WORKDIR_PATH=/opt/onnxruntime
WORKDIR $WORKDIR_PATH
ENV DEBIAN_FRONTEND noninteractive

ARG ORT_VERSION=1.11.0
ARG ORT_REPO=https://github.com/microsoft/onnxruntime.git
ARG OPENVINO_DEVICE=CPU_FP32 

ENV InferenceEngine_DIR=${INTEL_OPENVINO_DIR}/runtime/cmake

USER root
RUN apt update; apt install -y git protobuf-compiler libprotobuf-dev

#########################################################################################################################
###--------------------------------------------------BUILDING ONEDNN--------------------------------------------------###
#########################################################################################################################

RUN mkdir -p /opt/onednn/src && \
    mkdir -p /opt/onednn/build && \
    mkdir -p /opt/onednn/install && \
    git clone -b rls-v2.6 https://github.com/oneapi-src/oneDNN.git /opt/onednn/src
    
RUN cd /opt/onednn/build && \
	cmake -DCMAKE_INSTALL_PREFIX=/opt/onednn/install \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_GPU_RUNTIME=OCL \
		-DONEDNN_ENABLE_WORKLOAD=INFERENCE \
		-DONEDNN_BUILD_EXAMPLES=OFF \
		-DONEDNN_BUILD_TESTS=OFF\
		../src
RUN cd /opt/onednn/build && \
    make -j`nproc` && \
    make install
    
##########################################################################################################################
###--------------------------------------------------BUILDING ONNXRUNTIME----------------------------------------------###
##########################################################################################################################

RUN mkdir -p ${WORKDIR_PATH}/src && \
	mkdir -p ${WORKDIR_PATH}/build && \
	mkdir -p ${WORKDIR_PATH}/install && \
	git clone --recursive -b rel-${ORT_VERSION} ${ORT_REPO} ${WORKDIR_PATH}/src
	
RUN cd ${WORKDIR_PATH}/src && \
	/bin/sh dockerfiles/scripts/install_common_deps.sh && \
	ln -s cmake-* cmake-dir && \
	python3 -m pip install wheel

ENV PATH=${WORKDIR_PATH}/src/cmake-dir/bin:$PATH

RUN pip3 install onnx

RUN cd ${WORKDIR_PATH}/src && \
	./build.sh --config Release --update --build --parallel --build_shared_lib --build_wheel \
		--use_dnnl \
		--use_openvino ${DEVICE} \
		--build_dir ${WORKDIR_PATH}/build \
		--cmake_extra_defines CMAKE_INSTALL_PREFIX=${WORKDIR_PATH}/install ONNXRUNTIME_VERSION=$(cat ./VERSION_NUMBER) \
		--skip_tests --skip_submodule_sync
		
RUN cd ${WORKDIR_PATH}/build/Release && \
	make -j7 && \
	make install
	
 
RUN mkdir -p ${WORKDIR}/install/pywheels && \
	 cp -r ${WORKDIR_PATH}/build/Release/dist/*.whl ${WORKDIR}/install/pywheels

#Steps to download sources
RUN cat /etc/apt/sources.list | sed 's/^# deb-src/deb-src/g' > ./temp; mv temp /etc/apt/sources.list
RUN apt update; apt install dpkg-dev
RUN mkdir /sources
WORKDIR /sources
RUN apt-get source cron iso-codes libapt-inst2.0 lsb-release powermgmt-base python-apt-common python3-apt python3-dbus python3-gi unattended-upgrades libapt-pkg5.0 libhogweed4 libnettle6
WORKDIR /
RUN tar cvf GPL_sources.tar.gz /sources

##########################################################################################################################
###----------------------------------------------------BUILDING GEANT4-------------------------------------------------###
##########################################################################################################################

FROM ort_builder as g4_builder

ENV G4_VERSION 11.0

ARG G4_URL=https://gitlab.cern.ch/geant4/geant4.git

RUN apt-get update -y && \
    apt-get install -y libexpat1 libexpat1-dev
    
RUN mkdir -p /opt/geant4/src && \
    mkdir -p /opt/geant4/build && \
    mkdir -p /opt/geant4/install && \
    mkdir -p /opt/geant4/data && \
    git clone --recursive -b geant4-${G4_VERSION}-release ${G4_URL} /opt/geant4/src
    
RUN ls /opt/geant4/src
    
RUN cd /opt/geant4/build && \
    cmake -DCMAKE_INSTALL_PREFIX=/opt/geant4/install \
          -DGEANT4_INSTALL_DATA=ON \
          -DGEANT4_INSTALL_DATADIR=/opt/geant4/data \
          -DGEANT4_BUILD_MULTITHREADED=ON \
          -DGEANT4_INSTALL_EXAMPLES=OFF \
          -DGEANT4_USE_SYSTEM_EXPAT=ON \
          ../src && \
    make -j`nproc` && \
    make install
    
RUN source /opt/geant4/install/bin/geant4.sh

##############################################################################################################################
###-------------------------------------------------INSTALLING ROOT-CERN---------------------------------------------------###
##############################################################################################################################

FROM g4_builder as root_builder

ARG root_v=6.26.04
ARG os_v=Linux-ubuntu18
ARG architecure_v=x86_64
ARG gcc_v=gcc7.5

RUN mkdir -p /opt/root/src && \
	cd /opt/root/src && \
	wget https://root.cern/download/root_v${root_v}.${os_v}-${architecure_v}-${gcc_v}.tar.gz && \
	tar -xzvf root_v${root_v}.${os_v}-${architecure_v}-${gcc_v}.tar.gz && \
	source root/bin/thisroot.sh && \
	rm -rf root_v${root_v}.${os_v}-${architecure_v}-${gcc_v}.tar.gz

RUN cd /opt/root/src && \
	ls .

##############################################################################################################################
###-----------------------------------------------------BUILDING PAR04-----------------------------------------------------###
##############################################################################################################################

FROM root_builder as par04_builder

RUN mkdir -p /opt/par04/src

ARG PAR04_URL=https://gitlab.cern.ch/prmehta/geant4_par04.git

RUN git clone --recursive ${PAR04_URL} /opt/par04/src

RUN cd /opt/par04/src && \
    rm -rf CMakeCache.txt CMakeFiles examplePar04 Makefile && \
	cmake -DCMAKE_BUILD_TYPE="Debug" \
		  -DCMAKE_PREFIX_PATH="/opt/root/src/root;/opt/geant4/install;/opt/onnxruntime/install;/opt/onnxruntime/data;/opt/onednn/install/lib;/opt/intel/openvino_2022/runtime/lib;/opt/intel/openvino_2022/runtime/include" \
		  -DCMAKE_CXX_FLAGS_DEBUG="-ggdb3" \
		  .	

RUN cd /opt/par04/src && \
	make -j`nproc` && \
    make install
    
#############################################################################################################################
###-------------------------------------------------------CLEANING UP-----------------------------------------------------###
#############################################################################################################################

FROM openvino/ubuntu18_runtime:${OPENVINO_VERSION}

ENV DEBIAN_FRONTEND noninteractive
USER root

RUN apt-get update && \
	apt-get install -y vim

RUN mkdir -p /opt/onnxruntime/install && \
	mkdir -p /opt/geant4/install && \
	mkdir -p /opt/geant4/data && \
	mkdir -p /opt/Par04

COPY --from=ort_builder /opt/onednn/install /opt/onednn/install 
COPY --from=root_builder /opt/root/src/root /opt/root/src/root
COPY --from=ort_builder /opt/onnxruntime/install /opt/onnxruntime/install
COPY --from=g4_builder /opt/geant4/install /opt/geant4/install
COPY --from=g4_builder /opt/geant4/data /opt/geant4/data
COPY --from=par04_builder /opt/par04/src /opt/par04
COPY --from=ort_builder /sources /sources

ENV PATH=${WORKDIR_PATH}/miniconda/bin:${WORKDIR_PATH}/cmake-dir/bin:$PATH
ENV IE_PLUGINS_PATH=${INTEL_OPENVINO_DIR}/runtime/lib/intel64
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/onnxruntime/install/lib:/opt/intel/opencl:${INTEL_OPENVINO_DIR}/runtime/3rdparty/tbb/lib:${IE_PLUGINS_PATH}:/opt/onednn/install/include:/opt/onednn/install/lib

RUN echo ". /opt/geant4/install/bin/geant4.sh" >> ~/.bashrc
RUN echo ". /opt/root/src/root/bin/thisroot.sh" >> ~/.bashrc

RUN rm -rf /var/lib/apt/lists/*
#RUN cd /opt/onnxruntime/install && \
#	python3 -m pip install ./*.whl


WORKDIR /opt/par04
							   							  #####
###-------------------------------------------------------#END#-----------------------------------------------------------###
							   							  #####	



