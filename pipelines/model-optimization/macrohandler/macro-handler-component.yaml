name: Macro-Handler
description: Gives more control over macrofile creation and specifically inference component output path

inputs:
- {name: jsonlines_url, type: String, description: 'URL of Jsonlines macro file'}
- {name: jsonlines_path, type: String, description: 'Path to Jsonlines macro file'}
- {name: particle_energy, type: String, description: 'Energy of the incoming particles'}
- {name: particle_angle, type: Integer, description: 'Angle of the incoming particle'}
- {name: set_size_latent_vector, type: Integer, default: 10, description: 'dimension of the latent vector (encoded vector in a Variational Autoencoder model)'}
- {name: set_size_condition_vector, type: Integer, default: 4, description: 'size of the condition vector (energy, angle and geometry)'}
- {name: set_model_path_name, type: String, default: "./MLModels/Generator.onnx", description: 'path to the model which is set to download by cmake'}
- {name: set_profile_flag, type: Boolean, default: true, description: 'Whether to profile the model or not'}
- {name: set_optimization_flag, type: Boolean, default: false, description: 'Whether to perform graph optimization or not'}
- {name: set_dnnl_flag, type: Boolean, default: false, description: 'Whether to use ORT oneDNN/DNNL Execution Provider or not'}
- {name: set_openvino_flag, type: Boolean, default: false, description: 'Whether to use ORT OpenVINO Execution Provider or not'}
- {name: set_cuda_flag, type: Boolean, default: false, description: 'Whether to use ORT Cuda Execution Provider or not'}
- {name: set_tensorrt_flag, type: Boolean, default: false, description: 'Whether to use ORT TensorRT Execution Provider or not'}
- {name: set_dnnl_enable_cpu_mem_arena, type: Boolean, default: true, description: 'Whether to enable dynamic runtime memory for oneDNN/DNNL'}
- {name: set_cuda_device_id, type: Integer, default: 0, description: 'ID of the device to use CUDA on'}
- {name: set_cuda_gpu_mem_limit, type: String, default: '2147483648', description: 'GPU memory limit when using CUDA'}
- {name: set_cuda_arena_extended_strategy, type: String, default: 'kSameAsRequested', description: 'Strategy to be used for extending dynamic runtime memory for CUDA'}
- {name: set_cuda_cudnn_conv_algo_search, type: String, default: 'DEFAULT', description: 'Strategy to use for searching the best cudnn convolutional algorithm'}
- {name: set_cuda_do_copy_in_default_stream, type: Boolean, default: true, description: 'Whether to use same compute stream for copying data or not'}
- {name: set_cuda_cudnn_conv_use_max_workspace, type: Integer, default: 1, description: 'Whether to use maxmimum memory allocated to CUDA for computing convolution algorithm'}
- {name: set_trt_device_id, type: Integer, default: 0, description: 'ID of device to use TensorRT on'}
- {name: set_trt_max_workspace_size, type: String, default: '2147483648', description: 'GPU memory limit for computing an algorithm when using TensorRT'}
- {name: set_trt_max_partition_iterations, type: Integer, default: 10, description: 'Maximum number of iterations allowed in model partitioning for TensorRT'}
- {name: set_trt_min_subgraph_size, type: Integer, default: 5, description: 'Minimum node size in a subgraph after partitioning.'}
- {name: set_trt_fp16_enable, type: Boolean, default: false, description: 'Enable Fp16 mode in TensorRT'}
- {name: set_trt_int8_enable, type: Boolean, default: false, description: 'Enable INT8 mode in TensorRT'}
- {name: set_trt_int8_use_native_calibration_table, type: Boolean, default: true, description: 'Select what calibration table is used for non-QDQ models in INT8 mode. If 1, native TensorRT generated calibration table is used; if 0, ONNXRUNTIME tool generated calibration table is used.'}
- {name: set_trt_engine_cache_enable, type: Boolean, default: true, description: 'Enable TensorRT engine caching.'}
- {name: set_trt_engine_cache_path, type: String, default: "/opt/trt/geant4/cache", description: 'Specify path for TensorRT engine and profile files if ORT_TENSORRT_ENGINE_CACHE_ENABLE is 1, or path for INT8 calibration table file if ORT_TENSORRT_INT8_ENABLE is 1.'}
- {name: set_trt_dump_subgraphs, type: Integer, default: 1, description: 'Dumps the subgraphs that are transformed into TRT engines in onnx format to the filesystem.'}
- {name: set_ov_device_type, type: String, default: 'CPU_FP32', description: 'Type of Device to run OpenVINO on'}
- {name: set_ov_enable_vpu_fast_compile, type: Boolean, default: false, description: 'Whether to use OpenVINO fast compile for VPU devices only'}
- {name: set_ov_num_of_threads, type: Integer, default: 1, description: 'Number of computation threads to use for OpenVINO'}
- {name: set_ov_use_compiled_network, type: Boolean, default: false, description: 'Whether to directly import pre-compiled blobs if exists or dump a pre-compiled blob at the executable path.'}
- {name: set_ov_enable_opencl_throttling, type: Boolean, default: false, description: 'Whether to enable OpenCL queue throttling for GPU devices (reduces CPU utilization when using GPU).'}
- {name: set_inference_library, type: String, default: 'ONNX', description: 'Inference Accelerator to use for Fast Simulation'}
#- {name: set_filename, type: String, default: "10GeV_1000events_fastsim_onnx.root", description: 'Inference Output filepath'}
- {name: beam_on, type: Integer, default: 1000, description: 'Number of Events to run during /run/beamOn'}


outputs:
- {name: macro_file_path, type: String, description: 'Path to the output Macro file'}

implementation:
  container:
    image: gitlab-registry.cern.ch/prmehta/geant4_par04/macro-handler:latest
    # command is a list of strings (command-line arguments). 
    # The YAML language has two syntaxes for lists and you can use either of them. 
    # Here we use the "flow syntax" - comma-separated strings inside square brackets.
    command: [
      python3, 
      # Path of the program inside the container
      /opt/macroHandler/macro-handler.py,
      --jsonl_url, {inputValue: jsonlines_url},
      --input_path, {inputPath: jsonlines_path},
      --particleEnergy, {inputValue: particle_energy},
      --particleAngle, {inputValue: particle_angle},
      --setSizeLatentVector, {inputValue: set_size_latent_vector},
      --setSizeConditionVector, {inputValue: set_size_condition_vector},
      --setModelPathName, {inputValue: set_model_path_name},
      --setProfileFlag, {inputValue: set_profile_flag},
      --setOptimizationFlag, {inputValue: set_optimization_flag},
      --setDnnlFlag, {inputValue: set_dnnl_flag},
      --setOpenVinoFlag, {inputValue: set_openvino_flag},
      --setCudaFlag, {inputValue: set_cuda_flag},
      --setTensorrtFlag, {inputValue: set_tensorrt_flag},
      --setDnnlEnableCpuMemArena, {inputValue: set_dnnl_enable_cpu_mem_arena},
      --setCudaDeviceId, {inputValue: set_cuda_device_id},
      --setCudaGpuMemLimit, {inputValue: set_cuda_gpu_mem_limit},
      --setCudaArenaExtendedStrategy, {inputValue: set_cuda_arena_extended_strategy},
      --setCudaCudnnConvAlgoSearch, {inputValue: set_cuda_cudnn_conv_algo_search},
      --setCudaDoCopyInDefaultStream, {inputValue: set_cuda_do_copy_in_default_stream},
      --setCudaCudnnConvUseMaxWorkspace, {inputValue: set_cuda_cudnn_conv_use_max_workspace},
      --setTrtDeviceId, {inputValue: set_trt_device_id},
      --setTrtMaxWorkspaceSize, {inputValue: set_trt_max_workspace_size},
      --setTrtMaxPartitionIterations, {inputValue: set_trt_max_partition_iterations},
      --setTrtMinSubgraphSize, {inputValue: set_trt_min_subgraph_size},
      --setTrtFp16Enable, {inputValue: set_trt_fp16_enable},
      --setTrtInt8Enable, {inputValue: set_trt_int8_enable},
      --setTrtInt8UseNativeCalibrationTable, {inputValue: set_trt_int8_use_native_calibration_table},
      --setTrtEngineCacheEnable, {inputValue: set_trt_engine_cache_enable},
      --setTrtEngineCachePath, {inputPath: set_trt_engine_cache_path},
      --setTrtDumpSubgraphs, {inputValue: set_trt_dump_subgraphs},
      --setOVDeviceType, {inputValue: set_ov_device_type},
      --setOVEnableVpuFastCompile, {inputValue: set_ov_enable_vpu_fast_compile},
      --setOVNumOfThreads, {inputValue: set_ov_num_of_threads},
      --setOVUseCompiledNetwork, {inputValue: set_ov_use_compiled_network},
      --setOVEnableOpenCLThrottling, {inputValue: set_ov_enable_opencl_throttling},
      --setInferenceLibrary, {inputValue: set_inference_library},
#      --setFileName, {inputValue: set_filename},
      --beamOn, {inputValue: beam_on},
      --output_path, {outputPath: macro_file_path},
    ]