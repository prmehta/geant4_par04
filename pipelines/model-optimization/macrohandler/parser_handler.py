import argparse
from distutils.util import strtobool

def _deserialize_bool(s) -> bool:
    return strtobool(s) == 1

def parse_args():
    
    parser = argparse.ArgumentParser(prog="Jsonlines to Macro", description="Converts the input jsonlines file to macro file")
    
    parser.add_argument(
        "-url",
        "--jsonl_url",
        type=str,
        required=True,
        help="URL of JSONLines files"
    )
    parser.add_argument(
        "-ip",
        "--input_path",
        type=str,
        required=True,
        help="Input Jsonlines file path",
    )
    parser.add_argument(
        "-op",  
        "--output_path",
        type=str,
        required=True,
        help="Macro file output path",
    )
    parser.add_argument(
        "-pe",  
        "--particleEnergy",
        type=str,
        default="10 Gev",
        required=False,
        help="Energy of the incoming particles",
    )
    parser.add_argument(
        "-pa",  
        "--particleAngle",
        type=int,
        default=90,
        required=False,
        help="Angle of the incoming particles",
    )
    parser.add_argument( 
        "--setSizeLatentVector",
        type=int,
        default=10,
        required=False,
        help="Dimension of the latent vector (encoded vector in a Variational Autoencoder model)",
    )
    parser.add_argument( 
        "--setSizeConditionVector",
        type=int,
        default=4,
        required=False,
        help="size of the condition vector (energy, angle and geometry)",
    )
    parser.add_argument( 
        "--setModelPathName",
        type=str,
        default="./MLModels/Generator.onnx",
        required=False,
        help="path to the model which is set to download by cmake",
    )
    parser.add_argument(
        "--setProfileFlag",
        type=_deserialize_bool,
        default=True,
        required=False,
        choices=[True, False],
        help="Whether to profile the model or not",
    )
    parser.add_argument( 
        "--setOptimizationFlag",
        type=_deserialize_bool,
        default=False,
        required=False,
        choices=[True, False],
        help="Whether to perform graph optimization or not",
    )
    parser.add_argument( 
        "--setDnnlFlag",
        type=_deserialize_bool,
        default=False,
        required=False,
        choices=[True, False],
        help="Whether to use ORT oneDNN/DNNL Execution Provider or not",
    )
    parser.add_argument( 
        "--setOpenVinoFlag",
        type=_deserialize_bool,
        default=False,
        required=False,
        choices=[True, False],
        help="Whether to use ORT OpenVINO Execution Provider or not",
    )
    parser.add_argument( 
        "--setCudaFlag",
        type=_deserialize_bool,
        default=False,
        required=False,
        choices=[True, False],
        help="Whether to use ORT Cuda Execution Provider or not",
    )
    parser.add_argument(  
        "--setTensorrtFlag",
        type=_deserialize_bool,
        default=False,
        required=False,
        choices=[True, False],
        help="Whether to use ORT TensorRT Execution Provider or not",
    )
    parser.add_argument(  
        "--setDnnlEnableCpuMemArena",
        type=_deserialize_bool,
        default=True,
        required=False,
        choices=[True, False],
        help="Whether to enable dynamic runtime memory for oneDNN/DNNL",
    )
    parser.add_argument(  
        "--setCudaDeviceId",
        type=int,
        default=0,
        required=False,
        help="ID of the device to use CUDA on",
    )
    parser.add_argument( 
        "--setCudaGpuMemLimit",
        type=str,
        default="2147483648",
        required=False,
        help="GPU memory limit when using CUDA",
    )
    parser.add_argument(  
        "--setCudaArenaExtendedStrategy",
        type=str,
        default="kSameAsRequested",
        required=False,
        help="Strategy to be used for extending dynamic runtime memory for CUDA",
    )
    parser.add_argument( 
        "--setCudaCudnnConvAlgoSearch",
        type=str,
        default="DEFAULT",
        required=False,
        help="Strategy to use for searching the best cudnn convolutional algorithm",
    )
    parser.add_argument( 
        "--setCudaDoCopyInDefaultStream",
        type=_deserialize_bool,
        default=True,
        required=False,
        choices=[True, False],
        help="Whether to use same compute stream for copying data or not",
    )
    parser.add_argument(  
        "--setCudaCudnnConvUseMaxWorkspace",
        type=int,
        default=1,
        required=False,
        help="Whether to use maxmimum memory allocated to CUDA for computing convolution algorithm",
    )
    parser.add_argument(  
        "--setTrtDeviceId",
        type=int,
        default=0,
        required=False,
        help="ID of device to use TensorRT on",
    )
    parser.add_argument( 
        "--setTrtMaxWorkspaceSize",
        type=str,
        default="2147483648",
        required=False,
        help="GPU memory limit for computing an algorithm when using TensorRT",
    )
    parser.add_argument( 
        "--setTrtMaxPartitionIterations",
        type=int,
        default=10,
        required=False,
        help="Maximum number of iterations allowed in model partitioning for TensorRT",
    )
    parser.add_argument(  
        "--setTrtMinSubgraphSize",
        type=int,
        default=5,
        required=False,
        help="Minimum node size in a subgraph after partitioning.",
    )
    parser.add_argument( 
        "--setTrtFp16Enable",
        type=_deserialize_bool,
        default=False,
        required=False,
        choices=[True, False],
        help="Enable Fp16 mode in TensorRT",
    )
    parser.add_argument( 
        "--setTrtInt8Enable",
        type=_deserialize_bool,
        default=False,
        required=False,
        choices=[True, False],
        help="Enable INT8 mode in TensorRT",
    )
    parser.add_argument(  
        "--setTrtInt8UseNativeCalibrationTable",
        type=_deserialize_bool,
        default=True,
        required=False,
        choices=[True, False],
        help="Select what calibration table is used for non-QDQ models in INT8 mode. If 1, native TensorRT generated calibration table is used; if 0, ONNXRUNTIME tool generated calibration table is used.",
    )
    parser.add_argument( 
        "--setTrtEngineCacheEnable",
        type=_deserialize_bool,
        default=True,
        required=False,
        choices=[True, False],
        help="Enable TensorRT engine caching.",
    )
    parser.add_argument(
        "--setTrtEngineCachePath",
        type=str,
        default="/opt/trt/geant4/cache",
        required=False,
        help="Specify path for TensorRT engine and profile files if ORT_TENSORRT_ENGINE_CACHE_ENABLE is 1, or path for INT8 calibration table file if ORT_TENSORRT_INT8_ENABLE is 1.",
    )
    parser.add_argument( 
        "--setTrtDumpSubgraphs",
        type=int,
        default=1,
        required=False,
        help="Dumps the subgraphs that are transformed into TRT engines in onnx format to the filesystem.",
    )
    parser.add_argument( 
        "--setOVDeviceType",
        type=str,
        default="CPU_FP32",
        required=False,
        help="Type of Device to run OpenVINO on",
    )
    parser.add_argument(  
        "--setOVEnableVpuFastCompile",
        type=_deserialize_bool,
        default=False,
        required=False,
        choices=[True, False],
        help="Whether to use OpenVINO fast compile for VPU devices only",
    )
    parser.add_argument( 
        "--setOVNumOfThreads",
        type=int,
        default=1,
        required=False,
        help="Number of computation threads to use for OpenVINO",
    )
    parser.add_argument(  
        "--setOVUseCompiledNetwork",
        type=_deserialize_bool,
        default=False,
        required=False,
        choices=[True, False],
        help="Whether to directly import pre-compiled blobs if exists or dump a pre-compiled blob at the executable path.",
    )
    parser.add_argument(  
        "--setOVEnableOpenCLThrottling",
        type=_deserialize_bool,
        default=False,
        required=False,
        choices=[True, False],
        help="Whether to enable OpenCL queue throttling for GPU devices (reduces CPU utilization when using GPU).",
    )
    parser.add_argument(  
        "--setInferenceLibrary",
        type=str,
        default="ONNX",
        required=False,
        help="Inference Accelerator to use for Fast Simulation",
    )
    """
    parser.add_argument(  
        "--setFileName",
        type=str,
        default="10GeV_1000events_fastsim_onnx.root",
        required=False,
        help="Inference Output filepath",
    )
    """
    parser.add_argument(  
        "--beamOn",
        type=int,
        default=1000,
        required=False,
        help="Number of Events to run during /run/beamOn",
    )
    
    args = parser.parse_args()
    return args