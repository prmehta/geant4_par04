import kfp
from kfp.v2 import dsl
from kfp.v2.dsl import component
from kfp.v2.dsl import (
    Input,
    Output,
    Artifact,
    Dataset,
    OutputPath,
    InputPath,
)
import uproot
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import jensenshannon
from scipy.special import kl_div, rel_entr
from scipy.stats import chisquare
import re
import json
import os
import tarfile

@component(
    packages_to_install=["uproot", "awkward", "numpy", "matplotlib", "scipy", "json"],
    output_component_file="benchmark-component-v2.yaml",
)
def benchmark(
    input_path_a: InputPath(str),
    input_path_b: InputPath(str),
    particle_energy: str,
    particle_angle: int,
    use_log_for_y: bool,
    save_figures: bool,
    saveRootDir: OutputPath(str),
    output_path: OutputPath(str),
):
    
    os.makedirs(saveRootDir, exist_ok=True)
    
    def get_similarity_metrics(
        Sim_A,
        Sim_B
    ):
        metric_dict = dict()
        vals_SimA = Sim_A.to_numpy()[0]
        vals_SimB = Sim_B.to_numpy()[0]
        metric_dict["kl_div"] = rel_entr(vals_SimA, vals_SimB)
        metric_dict["js_div"] = jensenshannon(vals_SimA, vals_SimB)
        metric_dict["chi2"] = chisquare(vals_SimA, vals_SimB)
        return metric_dict
    
    def plot(
        sim_A,
        sim_B,
        truthE,
        truthAngle,
        observable,
        saveRootDir,
        yLogScale=True,
        saveFig=True, 
    ):
        """
        This function creates a plot for a specific observable to compare the full and the fast simulation. A ratio plot is also created as a subplot. 
        - fnameFullSim: file name (+path) of the full simulation (root file)
        - fnameFastSim: file name (+path) of the full simulation (root file)
        - observable : the name of the physics quantity to plot (you can get the names with .keys() ) 
        - truthE: energy of the incoming particle (the one specified in the macro file)
        - truthAngle: angle of the incoming particle
        """
        vals_SimA = sim_A['%s;1'%observable].to_numpy()[0]
        bins_SimA = sim_A['%s;1'%observable].to_numpy()[1]
        vals_SimB = sim_B['%s;1'%observable].to_numpy()[0]   
        bins_SimB = sim_B['%s;1'%observable].to_numpy()[1]
        fig, axes = plt.subplots(2, 1,figsize=(15,10), clear=True, sharex=True )
        axes[0].fill_between(bins_SimA,np.concatenate(([0],vals_SimA)) , label='SimA' , alpha=0.5, step="pre",color='black')  
        axes[0].fill_between(bins_SimB,np.concatenate(([0],vals_SimB)),label='SimB', alpha=0.5, step="pre", color='red'  ) 
        if(yLogScale):
            axes[0].set_yscale('log')
        axes[0].legend(loc='upper right')
        axes[0].set_ylabel('E [MeV]')
        axes[1].plot( bins_SimA, np.concatenate(([0],vals_SimB))/np.concatenate(([0],vals_SimA)), '-o', c='grey'  )
        axes[1].axhline(y=1)
        axes[1].set_ylim(0, 2)
        axes[1].set_ylabel('SimA/SimB')
        axes[1].set_xlabel(f' {observable} ') 
        axes[0].set_title(' $e^-$, %s [GeV], %s$^{\circ}$ ' %(truthE,truthAngle)  )
        if(saveFig):    
            fig.savefig(os.path.join(saveRootDir, "figs", observable ,f'{observable}_E_{truthE}_A_{truthAngle}.png'))
    
    metric_dict = dict()
    
    with uproot.open(input_path_a) as sim_A, uproot.open(input_path_b) as sim_B:
        same_keys = set(sim_A.keys()).intersection(sim_B.keys())
        for key in same_keys:
            if not re.match(r'^.*(mem|memory|events).*$', key, flags=re.I):
                plot(sim_A[key], sim_B[key], truthE=particle_energy,
                     truthAngle=particle_angle, observable=key, yLogScale=use_log_for_y,
                     saveFig=save_figures, savesRootDir=saveRootDir)
                metric_dict[key] = get_similarity_metrics(sim_A[key], sim_B[key])
                            
    with open(os.path.join(saveRootDir, "sim_metric.json"), "w") as simMetricFile:          
        json.dump(metric_dict, simMetricFile)
        
    with tarfile.open(output_path, "w:gz") as plot_tar:
        plot_tar.add(saveRootDir, arcname=os.path.basename(saveRootDir))
        