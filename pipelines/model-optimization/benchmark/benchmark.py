import uproot
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import jensenshannon
from scipy.special import kl_div, rel_entr
from scipy.stats import chisquare
import parser_handler
import re
import json
import os
import tarfile

def get_similarity_metrics(
    Orig_Sim,
    Gen_Sim,
):
    metric_dict = dict()
    vals_Sim_Orig = Orig_Sim.to_numpy()[0]
    vals_Sim_Gen = Gen_Sim.to_numpy()[0]
    metric_dict["kl_div"] = np.sum(kl_div(vals_Sim_Gen, vals_Sim_Orig)).tolist()
    metric_dict["js_div"] = np.sum(jensenshannon(vals_Sim_Gen, vals_Sim_Orig)).tolist()
    print(metric_dict)
    print("Similarity Measures calculated")
    return metric_dict  


def plot(
    orig_sim,
    gen_sim,
    truthE,
    truthAngle,
    observable,
    saveRootDir,
    yLogScale=True,
    saveFig=True, 
):
    """
    This function creates a plot for a specific observable to compare the full and the fast simulation. A ratio plot is also created as a subplot. 
    - fnameFullSim: file name (+path) of the full simulation (root file)
    - fnameFastSim: file name (+path) of the full simulation (root file)
    - observable : the name of the physics quantity to plot (you can get the names with .keys() ) 
    - truthE: energy of the incoming particle (the one specified in the macro file)
    - truthAngle: angle of the incoming particle
    """

    orig_sim_np = orig_sim.to_numpy()
    gen_sim_np = gen_sim.to_numpy()
    print("Original Sim: ", orig_sim_np)
    print("Generated Sim: ", gen_sim_np)
    
    vals_Sim_Orig = orig_sim_np[0]
    bins_Sim_Orig = orig_sim_np[1]
    print("Obtained Values and Bins of Original Input")
    vals_Sim_Gen = gen_sim_np[0]
    bins_Sim_Gen = gen_sim_np[1] 
    print("Obtained Values and Bins of Generated Input")
    fig, axes = plt.subplots(2, 1,figsize=(15,10), clear=True, sharex=True )
    axes[0].fill_between(bins_Sim_Orig,np.concatenate(([0],vals_Sim_Orig)) , label='Original Sim' , alpha=0.5, step="pre",color='black')  
    axes[0].fill_between(bins_Sim_Gen,np.concatenate(([0],vals_Sim_Gen)),label='Generated Sim', alpha=0.5, step="pre", color='red'  ) 
    if(yLogScale):
        axes[0].set_yscale('log')
    axes[0].legend(loc='upper right')
    axes[0].set_ylabel('E [MeV]')
    
    gen_by_orig = np.concatenate(([0],vals_Sim_Gen))/np.concatenate(([0],vals_Sim_Orig))
    gen_by_orig = np.where(np.isnan(gen_by_orig), 0, gen_by_orig)
    
    axes[1].plot( bins_Sim_Orig, gen_by_orig, '-o', c='grey'  )
    axes[1].axhline(y=1)
    axes[1].set_ylim(0, 2)
    axes[1].set_ylabel('Generated_Sim/Original_Sim')
    axes[1].set_xlabel(f' {observable} ') 
    axes[0].set_title(' $e^-$, %s [GeV], %s$^{\circ}$ ' %(truthE,truthAngle)  )
    print("Histograms plotted!!")
    if(saveFig):
        hist_save_path = os.path.join(saveRootDir, "figs", observable ,f'{observable}_E_{truthE}_A_{truthAngle}.png')
        hist_save_path = hist_save_path.replace(";", "_")
        os.makedirs(os.path.dirname(hist_save_path), exist_ok=True)    
        fig.savefig(hist_save_path)
        print(f"Histogram saved at {hist_save_path}")
        
def memoryPlotter(
    orig_sim,
    gen_sim,
    observable,
    truthE,
    truthAngle,
    saveRootDir,
    saveFig=True, 
):
    fig, axes = plt.subplots(1, 1,figsize=(15,10), clear=True, sharex=True )
    axes.plot(orig_sim, label="Original Sim")
    axes.plot(gen_sim, label="Generated Sim")
    axes.set_ylabel(f"Memory (MB)")
    axes.set_xlabel(f"No. of events")
    axes.set_title(f"{observable}")
    axes.legend(loc="upper right")
    if saveFig:
        plot_save_path = os.path.join(saveRootDir, "figs", observable ,f'{observable}_E_{truthE}_A_{truthAngle}.png')
        plot_save_path = plot_save_path.replace(";", "_")
        os.makedirs(os.path.dirname(plot_save_path), exist_ok=True)    
        fig.savefig(plot_save_path)
        print(f"Memory Plot saved at {plot_save_path}")
    
def branchToList(branch):
    return [single_event.tolist() if isinstance(single_event, np.ndarray) else single_event for single_event in np.array(branch).tolist()]
    
def metricCalculator(
    input_path_A,
    input_path_B,
    rootFilename,
    truthE,
    truthAngle,
    saveRootDir,
    yLogScale=True,
    saveFig=True,
):    
    metric_dict = dict()
    
    with uproot.open(os.path.join(input_path_A, rootFilename)) as sim_A, uproot.open(os.path.join(input_path_B, rootFilename)) as sim_B:
        same_keys = set(sim_A.keys()).intersection(sim_B.keys())
        print(same_keys)
        for key in same_keys:
            if re.match(r'^.*(mem|memory|events).*$', key, flags=re.I):
                print(f"Key: {key} -> No plotting or Similarity Measure calculation")
            else:
                print(f"Key: {key} -> Plotting and Similarity Measure calculation required")
                print("Plotting Histograms....")
                plot(sim_A[key], sim_B[key], truthE=truthE,
                     truthAngle=truthAngle, observable=key, yLogScale=yLogScale,
                     saveFig=saveFig, saveRootDir=saveRootDir)
                
                print("Calculating Similarity Measure....")
                metric_dict[key] = get_similarity_metrics(sim_A[key], sim_B[key])
                print("\n\n")
            
        event_quantities_key = set(sim_A["events"].keys()).intersection(sim_B["events"].keys())
        print(event_quantities_key)
        for event_quant in event_quantities_key:
            metric_dict[event_quant] = dict()
            if re.match(r'^.*(mem|memory).*$', event_quant, flags=re.I):
                print(f"Key: {event_quant} -> Memory Plot required")
                memoryPlotter(np.array(sim_A["events"][event_quant]), np.array(sim_B["events"][event_quant]),
                              observable=event_quant, truthE=truthE, truthAngle=truthAngle,
                              saveRootDir=saveRootDir, saveFig=saveFig)
                
            metric_dict[event_quant]["orig_sim"] = branchToList(sim_A["events"][event_quant])
            metric_dict[event_quant]["gen_sim"] = branchToList(sim_B["events"][event_quant])
            
    print("Writing output JSON file.....")
    with open(os.path.join(saveRootDir, "sim_metric.json"), "w") as simMetricFile:          
        json.dump(metric_dict, simMetricFile)
    print(f"JSON file written -> {os.path.join(saveRootDir, 'sim_metric.json')}")


if __name__ == '__main__':
    args = vars(parser_handler.parse_args())
    os.makedirs(args["saveRootDir"], exist_ok=True)
    metricCalculator(
        args["input_path_A"],
        args["input_path_B"],
        args["rootFilename"],
        args["truthE"],
        args["truthAngle"],
        args["saveRootDir"],
        args["yLogScale"],
        args["saveFig"],
    )
    