FROM python:slim-buster as baseImage

USER root

ARG WORKDIR_PATH=/opt/macroHandler

WORKDIR ${WORKDIR_PATH}

RUN pip install jsonlines requests numpy

COPY . ${WORKDIR_PATH}

CMD python3
