import kfp
from kfp.v2 import dsl
from benchmark_component import benchmark
from inference_component import inference
from ..inference.inferenceEnvVars import *
from macro_handler_component import macroHandler

@dsl.pipeline(
    name="Model Optimization Pipeline",
    description="KubeFlow pipeline for performing model optimization and memory \
                 profiling on simulation model for Geant4-FastSim using ONNXRuntime \
                 execution providers - oneDNN, CUDA and TensorRT"
)
def ModelOptimizationPipeline(
    fullSimJsonlUrl:str ="https://gitlab.cern.ch/prmehta/geant4_par04/-/raw/par04-gsoc-priyam/pipelines/model-optimization/macros/examplePar04.jsonl",
    fastSimJsonlUrl:str ="https://gitlab.cern.ch/prmehta/geant4_par04/-/raw/par04-gsoc-priyam/pipelines/model-optimization/macros/examplePar04_onnx.jsonl",
    jsonlSavePath:str ="/opt/macro.jsonl",
    particleEnergy:str ="10 GeV",
    particleAngle:int =90,
    setSizeLatentVector:int =10,
    setSizeConditionVector:int =4,
    setModelPathName:str ="./MLModels/Generator.onnx",
    setProfileFlag:int =1,
    setOptimizationFlag:int =0,
    setDnnlFlag:int =0,
    setOpenVinoFlag:int =0,
    setCudaFlag:int =0,
    setTensorrtFlag:int =0,
    setDnnlEnableCpuMemArena:int =1,
    setCudaDeviceId:int =0,
    setCudaGpuMemLimit:str ="2147483648",
    setCudaArenaExtendedStrategy:str ="kSameAsRequested",
    setCudaCudnnConvAlgoSearch:str ="DEFAULT",
    setCudaDoCopyInDefaultStream:int =1,
    setCudaCudnnConvUseMaxWorkspace:int =1,
    setTrtDeviceId:int =0,
    setTrtMaxWorkspaceSize:str ="2147483648",
    setTrtMaxPartitionIterations:int =10,
    setTrtMinSubgraphSize:int =5,
    setTrtFp16Enable:int =0,
    setTrtInt8Enable:int =0,
    setTrtInt8UseNativeCalibrationTable:int =1,
    setTrtEngineCacheEnable:int =1,
    setTrtEngineCachePath:str ="/opt/trt/geant4/cache",
    setTrtDumpSubgraphs:int =1,
    setOVDeviceType:str ="CPU_FP32",
    setOVEnableVpuFastCompile:int =0,
    setOVNumOfThreads:int =1,
    setOVUseCompiledNetwork:int =0,
    setOVEnableOpenCLThrottling:int =0,
    setInferenceLibrary:str ="ONNX",
#    setFileName:str ="/opt/10GeV_1000events_fastsim_onnx.root",
    beamOn:int =1000,
    yLogScale=True,
    saveFig=True,
#    saveRootDir:OutputPath(str) ="/opt",
):
    
    outRootfiles = list()
    
    for jsonlUrl in [fullSimJsonlUrl, fastSimJsonlUrl]:
        macro_handler_task = macroHandler(
            jsonlines_url=jsonlUrl,
            jsonlines_path=jsonlSavePath,
            particle_energy=particleEnergy,
            particle_angle=particleAngle,
            set_size_latent_vector=setSizeLatentVector,
            set_size_condition_vector=setSizeConditionVector,
            set_model_path_name=setModelPathName,
            set_profile_flag=setProfileFlag,
            set_optimization_flag=setOptimizationFlag,
            set_dnnl_flag=setDnnlFlag,
            set_openvino_flag=setOpenVinoFlag,
            set_cuda_flag=setCudaFlag,
            set_tensorrt_flag=setTensorrtFlag,
            set_dnnl_enable_cpu_mem_arena=setDnnlEnableCpuMemArena,
            set_cuda_device_id=setCudaDeviceId,
            set_cuda_gpu_mem_limit=setCudaGpuMemLimit,
            set_cuda_arena_extended_strategy=setCudaArenaExtendedStrategy,
            set_cuda_cudnn_conv_algo_search=setCudaCudnnConvAlgoSearch,
            set_cuda_do_copy_in_default_stream=setCudaDoCopyInDefaultStream,
            set_cuda_cudnn_conv_use_max_workspace=setCudaCudnnConvUseMaxWorkspace,
            set_trt_device_id=setTrtDeviceId,
            set_trt_max_workspace_size=setTrtMaxWorkspaceSize,
            set_trt_max_partition_iterations=setTrtMaxPartitionIterations,
            set_trt_min_subgraph_size=setTrtMinSubgraphSize,
            set_trt_fp16_enable=setTrtFp16Enable,
            set_trt_int8_enable=setTrtInt8Enable,
            set_trt_int8_use_native_calibration_table=setTrtInt8UseNativeCalibrationTable,
            set_trt_engine_cache_enable=setTrtEngineCacheEnable,
            set_trt_engine_cache_path=setTrtEngineCachePath,
            set_trt_dump_subgraphs=setTrtDumpSubgraphs,
            set_ov_device_type=setOVDeviceType,
            set_ov_enable_vpu_fast_compile=setOVEnableVpuFastCompile,
            set_ov_num_of_threads=setOVNumOfThreads,
            set_ov_use_compiled_network=setOVUseCompiledNetwork,
            set_ov_enable_opencl_throttling=setOVEnableOpenCLThrottling,
            set_inference_library=setInferenceLibrary,
            beam_on=beamOn,
        )
        
        inference_task = inference(macro_file=macro_handler_task.outputs["macro_file_path"]) \
                                    .set_gpu_limit(1) \
                                    .add_env_variable(INF_PATH) \
                                    .add_env_variable(INF_LD_LIBRARY_PATH) \
                                    .add_env_variable(INF_G4ENSDFSTATEDATA) \
                                    .add_env_variable(INF_G4PIIDATA) \
                                    .add_env_variable(INF_TBB_DIR) \
                                    .add_env_variable(INF_G4INCLDATA) \
                                    .add_env_variable(INF_InferenceEngine_DIR) \
                                    .add_env_variable(INF_OpenVINO_DIR) \
                                    .add_env_variable(INF_G4PARTICLEXSDATA) \
                                    .add_env_variable(INF_INTEL_OPENVINO_DIR) \
                                    .add_env_variable(INF_OpenCV_DIR) \
                                    .add_env_variable(INF_G4NEUTRONHPDATA) \
                                    .add_env_variable(INF_G4SAIDXSDATA) \
                                    .add_env_variable(INF_ngraph_DIR) \
                                    .add_env_variable(INF_G4REALSURFACEDATA) \
                                    .add_env_variable(INF_G4ABLADATA) \
                                    .add_env_variable(INF_G4LEVELGAMMADATA) \
                                    .add_env_variable(INF_G4RADIOACTIVEDATA) \
                                    .add_env_variable(INF_HDDL_INSTALL_DIR)
        
        outRootfiles += [inference_task.outputs["output_root_file"]]
        
    benchmark_results = benchmark(
        input_path_a=outRootfiles[0],
        input_path_b=outRootfiles[1],
        particle_energy=particleEnergy,
        particle_angle=particleAngle,
        use_log_for_y=yLogScale,
        save_figures=saveFig,
    )
                                                                                                                
if __name__ == '__main__':
    kfp.compiler.Compiler().compile(
        pipeline_func=ModelOptimizationPipeline,
        package_path="model-optimization-pipeline.yaml"
    ) 
        