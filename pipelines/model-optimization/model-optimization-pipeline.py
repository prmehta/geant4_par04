import kfp
from inputChecker.inputchecker import inputChecker
from helpers import *

@kfp.dsl.pipeline(
    name="Geant4-FastSim Memory Footprint Optimization Pipeline",
    description="KubeFlow pipeline for performing model optimization and memory \
                 profiling on simulation model for Geant4-FastSim using ONNXRuntime \
                 execution providers - oneDNN, CUDA and TensorRT",
)
def ModelOptimizationPipeline(
    fullSimJsonlUrl:str ="https://gitlab.cern.ch/prmehta/geant4_par04/-/raw/par04-gsoc-priyam/pipelines/model-optimization/macros/examplePar04.jsonl",
    fastSimJsonlUrl:str ="https://gitlab.cern.ch/prmehta/geant4_par04/-/raw/par04-gsoc-priyam/pipelines/model-optimization/macros/examplePar04_onnx.jsonl",
    jsonlSavePath:str ="/opt/macro.jsonl",
    particleEnergy:str ="10 GeV",
    particleAngle:int =90,
    setSizeLatentVector:int =10,
    setSizeConditionVector:int =4,
    setModelPathName:str ="./MLModels/Generator.onnx",
    setProfileFlag:bool =True,
    setOptimizationFlag:bool =False,
    setDnnlFlag:bool =False,
    setOpenVinoFlag:bool =False,
    setCudaFlag:bool =False,
    setTensorrtFlag:bool =False,
    setDnnlEnableCpuMemArena:bool =True,
    setCudaDeviceId:int =0,
    setCudaGpuMemLimit:str ="2147483648",
    setCudaArenaExtendedStrategy:str ="kSameAsRequested",
    setCudaCudnnConvAlgoSearch:str ="DEFAULT",
    setCudaDoCopyInDefaultStream:bool =True,
    setCudaCudnnConvUseMaxWorkspace:int =1,
    setTrtDeviceId:int =0,
    setTrtMaxWorkspaceSize:str ="2147483648",
    setTrtMaxPartitionIterations:int =10,
    setTrtMinSubgraphSize:int =5,
    setTrtFp16Enable:bool =False,
    setTrtInt8Enable:bool =False,
    setTrtInt8UseNativeCalibrationTable:bool =True,
    setTrtEngineCacheEnable:bool =True,
    setTrtEngineCachePath:str ="/opt/trt/geant4/cache",
    setTrtDumpSubgraphs:int =1,
    setOVDeviceType:str ="CPU_FP32",
    setOVDeviceId:int =0,
    setOVEnableVpuFastCompile:bool =False,
    setOVNumOfThreads:int =1,
    setOVUseCompiledNetwork:bool =False,
    setOVEnableOpenCLThrottling:bool =False,
    setInferenceLibrary:str ="ONNX",
    setFileName:str ="10GeV_1000events_fastsim_onnx.root",
    beamOn:int =1000,
    yLogScale:bool =True,
    saveFig:bool =True,
    num_of_gpus:int =0,
):
    
    inputChecker( 
        particle_energy=particleEnergy,
        particle_angle=particleAngle,
        set_size_latent_vector=setSizeLatentVector,
        set_size_condition_vector=setSizeConditionVector,
        set_model_path_name=setModelPathName,
        set_profile_flag=setProfileFlag,
        set_optimization_flag=setOptimizationFlag,
        set_dnnl_flag=setDnnlFlag,
        set_openvino_flag=setOpenVinoFlag,
        set_cuda_flag=setCudaFlag,
        set_tensorrt_flag=setTensorrtFlag,
        set_dnnl_enable_cpu_mem_arena=setDnnlEnableCpuMemArena,
        set_cuda_device_id=setCudaDeviceId,
        set_cuda_gpu_mem_limit=setCudaGpuMemLimit,
        set_cuda_arena_extended_strategy=setCudaArenaExtendedStrategy,
        set_cuda_cudnn_conv_algo_search=setCudaCudnnConvAlgoSearch,
        set_cuda_do_copy_in_default_stream=setCudaDoCopyInDefaultStream,
        set_cuda_cudnn_conv_use_max_workspace=setCudaCudnnConvUseMaxWorkspace,
        set_trt_device_id=setTrtDeviceId,
        set_trt_max_workspace_size=setTrtMaxWorkspaceSize,
        set_trt_max_partition_iterations=setTrtMaxPartitionIterations,
        set_trt_min_subgraph_size=setTrtMinSubgraphSize,
        set_trt_fp16_enable=setTrtFp16Enable,
        set_trt_int8_enable=setTrtInt8Enable,
        set_trt_int8_use_native_calibration_table=setTrtInt8UseNativeCalibrationTable,
        set_trt_engine_cache_enable=setTrtEngineCacheEnable,
        set_trt_engine_cache_path=setTrtEngineCachePath,
        set_trt_dump_subgraphs=setTrtDumpSubgraphs,
        set_ov_device_type=setOVDeviceType,
        set_ov_enable_vpu_fast_compile=setOVEnableVpuFastCompile,
        set_ov_num_of_threads=setOVNumOfThreads,
        set_ov_use_compiled_network=setOVUseCompiledNetwork,
        set_ov_enable_opencl_throttling=setOVEnableOpenCLThrottling,
        set_inference_library=setInferenceLibrary,
        beam_on=beamOn,
    )
    
    outRootfilesdir = list()
    macro_handler = get_macro_handler(url=False)
#    with kfp.dsl.ParallelFor([fullSimJsonlUrl, fastSimJsonlUrl]) as jsonlUrl:
    for jsonlUrl, sim_type in [(fullSimJsonlUrl, "Fullsim"), (fastSimJsonlUrl, "OnnxFastSimNoEP")]:
        macro_handler_task = macro_handler(
            jsonlines_url=jsonlUrl,
            jsonlines_path=jsonlSavePath,
            particle_energy=particleEnergy,
            particle_angle=particleAngle,
            set_size_latent_vector=setSizeLatentVector,
            set_size_condition_vector=setSizeConditionVector,
            set_model_path_name=setModelPathName,
            set_profile_flag=setProfileFlag,
            set_optimization_flag=setOptimizationFlag,
            set_dnnl_flag=setDnnlFlag,
            set_openvino_flag=setOpenVinoFlag,
            set_cuda_flag=setCudaFlag,
            set_tensorrt_flag=setTensorrtFlag,
            set_dnnl_enable_cpu_mem_arena=setDnnlEnableCpuMemArena,
            set_cuda_device_id=setCudaDeviceId,
            set_cuda_gpu_mem_limit=setCudaGpuMemLimit,
            set_cuda_arena_extended_strategy=setCudaArenaExtendedStrategy,
            set_cuda_cudnn_conv_algo_search=setCudaCudnnConvAlgoSearch,
            set_cuda_do_copy_in_default_stream=setCudaDoCopyInDefaultStream,
            set_cuda_cudnn_conv_use_max_workspace=setCudaCudnnConvUseMaxWorkspace,
            set_trt_device_id=setTrtDeviceId,
            set_trt_max_workspace_size=setTrtMaxWorkspaceSize,
            set_trt_max_partition_iterations=setTrtMaxPartitionIterations,
            set_trt_min_subgraph_size=setTrtMinSubgraphSize,
            set_trt_fp16_enable=setTrtFp16Enable,
            set_trt_int8_enable=setTrtInt8Enable,
            set_trt_int8_use_native_calibration_table=setTrtInt8UseNativeCalibrationTable,
            set_trt_engine_cache_enable=setTrtEngineCacheEnable,
            set_trt_engine_cache_path=setTrtEngineCachePath,
            set_trt_dump_subgraphs=setTrtDumpSubgraphs,
            set_ov_device_type=setOVDeviceType,
            set_ov_enable_vpu_fast_compile=setOVEnableVpuFastCompile,
            set_ov_num_of_threads=setOVNumOfThreads,
            set_ov_use_compiled_network=setOVUseCompiledNetwork,
            set_ov_enable_opencl_throttling=setOVEnableOpenCLThrottling,
            set_inference_library=setInferenceLibrary,
            beam_on=beamOn,
        ).set_display_name(f"{sim_type}MacroHandler")

        inference_task = configure_inference_task(
            macro_file=macro_handler_task.outputs["macro_file_path"],
            output_root_filename=setFileName,
            use_url=False,
            component_display_name=sim_type,
            gpu_limit=1 if sim_type == "OnnxFastSimNoEP" else None,
        )
                                    
        outRootfilesdir += [inference_task.outputs["output_directory"]]
    
    benchmark = get_benchmark(url=False)
    benchmark_task = benchmark(
        input_path_a=outRootfilesdir[0],
        input_path_b=outRootfilesdir[1],
        root_filename=setFileName,
        particle_energy=particleEnergy,
        particle_angle=particleAngle,
        use_log_for_y=yLogScale,
        save_figures=saveFig,
    ).set_display_name("FullSimV/SOnnxFastSimNoEP")
    
    return benchmark_task.outputs["output_root_directory"]
    

if __name__ == '__main__':
    kfp.compiler.Compiler().compile(
        pipeline_func=ModelOptimizationPipeline,
        package_path="model-optimization-pipeline.yaml"
    )
    
    
    

    
    
    