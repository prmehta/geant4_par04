import argparse
from distutils.util import strtobool
from secrets import choice

def _deserialize_bool(s) -> bool:
    return strtobool(s) == 1

def parse_args():
    
    parser = argparse.ArgumentParser(prog="Benchmark", description="Benchmark or Validate the model using the metrics calculated by the program")
    
    parser.add_argument(
        "-ipa",
        "--input_path_A",
        type=str,
        required=True,
        help="Path of input A - root file",
    )
    parser.add_argument(
        "-ipb",
        "--input_path_B",
        type=str,
        required=True,
        help="Path of input B - root file",
    )
    parser.add_argument(
        "-rf",
        "--rootFilename",
        type=str,
        required=True,
        help="Common name of the output root files of both the inference component",
    )
    parser.add_argument(
        "-te",
        "--truthE",
        type=str,
        required=True,
        help="energy of the incoming particle (the one specified in the macro file)",
    )
    parser.add_argument(
        "-ta",
        "--truthAngle",
        type=str,
        required=True,
        help="angle of the incoming particle",
    )
    parser.add_argument(
        "-yl",
        "--yLogScale",
        type=_deserialize_bool,
        default=True,
        required=False,
        choices=[True, False],
        help="Whether to set y scale of plot to 'log'",
    )
    parser.add_argument(
        "-sf",
        "--saveFig",
        type=_deserialize_bool,
        default=True,
        required=False,
        choices=[True, False],
        help="Whether to save figures or not",
    )
    parser.add_argument(
        "-sfr",
        "--saveRootDir",
        type=str,
        default="/opt",
        required=False,
        help="Path to root directory for saving output",
    )
    
    args = parser.parse_args()
    
    return args